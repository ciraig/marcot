import pandas as pd
import numpy as np
import re
from scipy.optimize import minimize
import warnings
import copy
import ipdb
import pdb

try:
    from marcot import layers
    from marcot import core
except ModuleNotFoundError:
    import layers
    import core

class ProcessModel(object):
    """ A generic class for representing a technology adapting itself to
    changes in the composition of its inputs """

    def __init__(self, process_id, **kwargs):
        """

        Args
        ----

        Layers:
        * lay_min|max: bounds to product compositions acceptable for technology
        * lay_f_min|max: bounds to emission compositions acceptable for
          technology

        Transfer coefficients and technologies:
        * tc_min|max: bounds on the transfer coefficients
            * set equal for fixed transfer coefficients
        * a0: initial/base product flows
        * f0: initial/base extensions



        """
        # ---------------------------- HIDDEN ------------------------------------------------------------------------
        # Hardcoded
        self.__mandatory_l_cols = {'name', 'reference_property', 'unit'}
        self.__nes = ' n.e.s.'  # not elsewhere specified
        self.__mass_name = 'mass [kg]'
        self.__mass_marker = '[kg]'
        #self.__tc_columns = ['cons', 'lay', 'pro_in', 'str_in', 'pro_out', 'str_out', 'value']
        self.__tc_columns = ['cons','subst','goods','goods_out','value']

        # encapsulated, "protected" values
        self.__lays_affect_tech = None
        self.__l_pro = None
        self.__determining_goods_flows = None


        # ----------------------- KEY VARIABLES ----------------------------------------------------------------------
        # The id of the process, it has to know its name
        self.process_id = process_id

        # What we are trying to determine
        self.u = pd.Series()
        self.v = pd.Series()
        self.f = pd.Series()
        self.lay = None
        self.lay_f = None

        # Initial product/extension flows
        self.u0 = pd.Series()  # initial use flows
        self.v0 = pd.Series()  # initial supply flows
        self.f0 = pd.Series()  # initial env extensions

        # Initialize Layers
        self.lay0 = layers.Layers()
        self.lay_f0 = layers.Layers()

        # Transfer coefficients TODO: make a decision on this once and for all
        #self.tc = pd.DataFrame(columns=self.__tc_columns)k
        self.tc_goods = None
        self.tc_subst = None
        self.tc_cons = None

        # Technical coefficients # TODO: va devenir te
        self.pc = pd.DataFrame()
        self.te = self.pc

        # --------- CONSTRAINTS --------------------------------------------------------------------------------------

        # Constraints to product and extension compositions
        self.lay_min_subst = None
        self.lay_max_subst = None
        self.lay_min_goods = None
        self.lay_max_goods = None
        self.lay_f_min = None
        self.lay_f_max = None

        # Constraints on Transfer coefficients
        self.tc_min = pd.DataFrame()
        self.tc_max = pd.DataFrame()
        self.tc_f_min = pd.DataFrame()
        self.tc_f_max = pd.DataFrame()

        # Constraints on product use (u) and supply (v) flows
        self.u_min = None
        self.u_max = None
        self.v_min = None
        self.v_max = None
        self.f_min = None
        self.f_max = None

        # ----------- OTHERS -----------------------------------------------------------------------------------------
        # Labels
        self.l_str = pd.DataFrame()  # label/properties of elementary flows



        # Update all attributes based on kwargs
        # https://stackoverflow.com/a/54099816/7382307
        allowed_keys = [i for i in dir(self) if "__" not in i and any([j.endswith(i) for j in self.__dict__.keys()])]
        self.__dict__.update((key, value) for key, value in kwargs.items() if key in allowed_keys)
        rejected_keys = set(kwargs.keys()) - set(allowed_keys)
        if rejected_keys:
            raise ValueError("Invalid arguments in constructor:{}".format(rejected_keys))

        # Use initial compositions to initialize composition
        # TODO: problem with this is that this only works at initialization. Better to copy lay0 to lay "just in time"
        if self.lay is None:
            self.lay = copy.deepcopy(self.lay0)
        if self.lay_f is None:
            self.lay_f = copy.deepcopy(self.lay_f0)


    # ==========================================================================
    """ Dimensions """

    # ==========================================================================
    # which product compositions affect the technology

    @property
    def _pro(self):
        """ Returns number of types of product flows

        Deduces it from the dimensions of u, u0, v, or v0, in that order
        """
        pro = 0
        if not core.pd_isnull(self.u):
            pro = self.u.shape[0]
        elif not core.pd_isnull(self.u0):
            pro = self.u0.shape[0]
        elif not core.pd_isnull(self.v):
            pro = self.v.shape[0]
        elif not core.pd_isnull(self.v0):
            pro = self.v0.shape[0]
        else:
            print("Warning, pro not defined.")
        return pro

    @property
    def _str(self):
        # TODO: change stupid name!
        """ Returns the number of types  of stressors

        Deduces it from the dimensions of f or f0, in that order
        """
        if not core.pd_isnull(self.f):
            f = self.f.shape[0]
        elif not core.pd_isnull(self.f0):
            f = self.f0.shape[0]
        else:
            f = 0
        return f

    @property
    def _subst(self):
        """ Returns the number of substances in substance layer

        Deduces it from lay, lay0, lay_f or lay_f0, in that order
        """
        # TODO: Check whether this makes sense at all. Danger of having a
        # conflicting number of substances if more than one defined!

        # TODO: also, seems like something the Layer() object should be able to
        # handle

        if not core.pd_isnull(self.lay.subst):
            lay = self.lay.subst.shape[0]
        elif not core.pd_isnull(self.lay0.subst):
            lay = self.lay0.subst.shape[0]
        elif not core.pd_isnull(self.lay_f.subst):
            lay = self.lay_f.subst.shape[0]
        elif not core.pd_isnull(self.lay_f0.subst):
            lay = self.lay_f0.subst.shape[0]
        else:
            lay = 0
            print("Warning! _subst is not defined.")
        return lay

    # ==========================================================================
    """ Properties """

    # ==========================================================================

    @property
    def determining_goods_flows(self):
        # TODO: One day, we want much more than that...

        if self.__determining_goods_flows is not None:
            return self.__determining_goods_flows
        else:
            # TODO "Temporary fix, assumes no negative mass"
            out = []

            # Find probablem inputs (negative supply, positive use)
            v0 = pd.to_numeric(self.v0, errors='force')
            out += list(v0[v0 < 0].index)

            u0 = pd.to_numeric(self.u0, errors='force')
            out += list(u0[u0 > 0].index)


            # Find placeholder probable inputs
            try:
                out = out + list(self.v0[self.v0 == '-x'].index)
            except TypeError:
                pass

            try:
                out = out + list(self.u0[self.u0 == 'x'].index)
            except TypeError:
                pass

            return out

    @determining_goods_flows.setter
    def determining_goods_flows(self, determining_goods_flows):
        self.__determining_goods_flows = determining_goods_flows

    @property
    def l_pro(self):
        """ Metadata on products and wastes """
        return self.__l_pro

    @l_pro.setter
    def l_pro(self, df):
        """ Set metadata on products and wastes """
        try:
            cols = set(df.columns)
        except:
            raise ValueError("l_pro must be a pandas dataframe")

        if cols.issuperset(self.__mandatory_l_cols):
            self.__l_pro = df
        else:
            raise ValueError("l_pro must at least have these columns: " +
                             ', '.join(self.__mandatory_l_cols))

        # ------------------------------------------------------------------------------
    @property
    def vmu(self):
        """ Returns supply minus use """
        vmu = self.v.sub(self.u, fill_value = 0)
        return vmu[vmu !=0]


    # ==========================================================================
    """ Methods """

    # ==========================================================================
    def initialize_final_sut_and_layers(self):
        """ Simply copies over to the final variables the initial flows and compositions"""
        self.u = self.u0.copy(deep=True)
        self.v = self.v0.copy(deep=True)
        self.f = self.f0.copy(deep=True)
        self.lay = copy.deepcopy(self.lay0)
        self.lay_f = copy.deepcopy(self.lay_f0)

    def solve(self):
        """ Resolves the flows and compositions of process as a function of the parameters and compositions of inputs


        """

        # No composition data, nothing to do
        if self.lay0.isempty() and self.lay_f0.isempty():
            self.initialize_final_sut_and_layers()

        else:
            # All parametrization through transfer coefficients
            if (len(self.tc_goods) + len(self.tc_subst) + len(self.tc_cons)) and self.pc.empty:
                # TODO: maybe not need this
                self.lay = copy.deepcopy(self.lay0)
                self.apply_tc()

            # More complex parametrization
            else:
                raise NotImplementedError("No appropriate default rebalancing method found. You need to override the solve() method in a child class")


    @staticmethod
    def calc_inputs_and_outputs(u, v, f, lay, lay_f, f_convention='factor_input'):
        """ Distinguish physical inputs from a SUT inventory


        """

        def the_sorting(x, alay):

            # The net physical properties of scaled flows, are they positive or negative?
            phys_flow = np.sum(alay.net_substp().multiply(x, axis=1))

            # separate inputs or outputs by masking out opposite signs
            # internally, this subfunction follows the sign convention of Use table or Factor inputs
            ins =  x.mask(phys_flow <= 0).fillna(0.)  # inputs are positive flows
            outs = x.mask(phys_flow >= 0).fillna(0.) # outputs are negative flows
            return ins, -1 * outs

        su = set(u.index)
        sv = set(v.index)
        if su != sv:
            ix = su | sv
            u = u.reindex(ix)
            v = v.reindex(ix)

        # Clean up (remove any string --- '+', '-', '?' --- or NaN)
        u = pd.to_numeric(u, errors='coerce').fillna(0.)
        v = pd.to_numeric(v, errors='coerce').fillna(0.)
        f = pd.to_numeric(f, errors='coerce').fillna(0.)

        # Apply sorting
        inputs, outputs = the_sorting(u - v, lay)
        if f_convention == 'factor_input':
            inputs_f, outputs_f = the_sorting(f, lay_f)
        elif f_convention == 'emissions_output':
            inputs_f, outputs_f = the_sorting(-1 * f, lay_f)
        else:
            raise ValueError('Not a sign convention') 

        return inputs, inputs_f, outputs, outputs_f

    @staticmethod
    def calc_net_outputs(cons_out, subst_out, goods_out, alay):
        """ Converts output flows into _net_ output flows in each measurement unit

        In other words, converts complementary measures of out flows (some calculated in terms of goods out, others in
        mass and element outputs, etc.), and calculates (overlapping) net outputs for each unit.

        Parameters
        ----------
        cons_out : Dataframe [cons x outputs]
            outflows measured/calculated in terms of conserved properties
        subst_out : Dataframe [subst x outputs]
            outflows measured/calculated in terms of substances properties
        goods_out : Dataframe [subst x outputs]
            outflows measured/calculated in terms of goods
        alay : Layer() object

        Returns
        -------
        net_cons_out: Dataframe
            The net total amount of each conserved property going to each outflow
        net_subst_out: Dataframe
            The net total amount of each non-conserved property (substance) going to each outflow
        net_goods_out: Dataframe
            The net total amount of each good going to each outflow

        """
        net_goods_out = core.dot(alay.net_goods(), goods_out)
        net_subst_out = core.dot(alay.net_substp(), goods_out).add(subst_out, fill_value=0.)
        net_cons_out  = core.dot(alay.net_consp(), goods_out
                                ).add(core.dot(alay.consp, subst_out), fill_value=0.
                                ).add(cons_out, fill_value=0.)
        return net_cons_out, net_subst_out, net_goods_out


    def set_default_constraints(self):
        """ Sets commonsense constraints on process flows and layers

        Unless they are previously defined, method strives to sets:
            * u_min|max based on u0
            * v_min|v_max
            * v_max
            * f_min
            * f_max
            * lay_min
            * lay_max
            * lay_f_min
            * lay_f_max

        Based on basic rules:
        * No sign change (no reversal of flows) relative to original data
        * No new flows/compositions (if value exactly at 0.0, leave it at that)

        # N.B. What is the difference between lay_bound and lay_max/min?
        #   lay_bound takes reference property into account,
        #   limitting the concentration of that property to unity

        """

        # ----------- Product flow bounds ---------------
        self.u_min, self.u_max = self.make_bounds(self.u, r0=self.u0)
        self.v_min, self.v_max = self.make_bounds(self.v, r0=self.v0)
        self.lay_min_subst, self.lay_max_subst = self.make_bounds(self.lay.subst, r0=self.lay0.subst)
        self.lay_min_goods, self.lay_max_goods = self.make_bounds(self.lay.goods, r0=self.lay0.goods)

        # TODO: revive
        self.lay_bound = self._define_lay_bounds(self.lay, self.l_pro)

        # ----------- Factor flow bounds ---------------
        self.f_min, self.f_max = self.make_bounds(self.f, r0=self.f0)
        self.lay_f_min, self.lay_f_max = self.make_bounds(self.lay_f.subst, r0=self.lay_f0.subst)

        self.lay_f_bound = self._define_lay_bounds(self.lay_f, self.l_str)

       # TODO: fix!
       # if self.tc_max is None and self.tc_min is None and self.tc is not None:
       #     self.tc_min, self.tc_max = self.make_bounds(self.tc, strict=True, norm=True)



    def rebalance(self):
        """ Set u, v, and lay to rebalance the process, respecting constraints
        and minimizing changes to initial description"""

        def ravel_var(v, u, f, lay, lay_f):
            """ put all optimisable variables in one vector """

            if f is not None and lay_f is not None:
                x = np.concatenate((np.ravel(v),
                                    np.ravel(u),
                                    np.ravel(f),
                                    np.ravel(lay),
                                    np.ravel(lay_f)))
            else:
                x = np.concatenate((np.ravel(v),
                                    np.ravel(u),
                                    np.ravel(lay)))
            return x

        def unravel_sut(x, with_labels=False):
            """ extract all optimisable variables as SUT from vector"""

            v = x[:self._pro]
            _n = 2 * self._pro
            u = x[self._pro: _n]
            _o = _n + self._str
            f = x[_n: _o]
            _p = _o + self._subst * self._pro
            lay = x[_o: _p].reshape((-1, self._pro))

            lay_f = x[_p:]
            if len(lay_f):
                lay_f = lay_f.reshape((-1, self._str))

            if with_labels:
                v = pd.Series(v, self.l_pro.index)
                u = pd.Series(u, self.l_pro.index)
                lay = pd.DataFrame(lay, index=self.lay.subst.index, columns=self.l_pro.index)
                if len(f):
                    f = pd.Series(f, self.l_str.index)
                    lay_f = pd.DataFrame(lay_f, index=self.lay_f.subst.index, columns=self.l_str.index)
                else:
                    f = lay_f = pd.DataFrame()


            return v, u, f, lay, lay_f

        def unravel_var(x):
            """ extract all optimizable variables as V-U and compositions"""
            v, u, f, lay, lay_f = unravel_sut(x)
            vmu = v - u
            return vmu, f, lay, lay_f

        # Objective functions
        # --------------------------------------------------------------------

        def objective_func_w_lay_out(x):
            """ Minimize sum of square of relative differences, for u, v, f, lay, and lay_f
            """
            v, u, f, lay, lay_f = unravel_sut(x)
            delta_v = np.sum(((self.v0.values - v) * core.one_over(self.v0)) ** 2)
            delta_u = np.sum(((self.u0.values - u) * core.one_over(self.u0)) ** 2)
            delta_f = np.sum(((self.f0.values - f) * core.one_over(self.f0)) ** 2)
            delta_lay = np.sum(((self.lay0.subst.values - lay) * core.one_over(self.lay0.subst.values)) ** 2)
            delta_lay_f = np.sum(((self.lay_f0.subst.values - lay_f) * core.one_over(self.lay_f0.subst.values)) ** 2)
            return delta_v + delta_u + delta_f + delta_lay + delta_lay_f

        def objective_func_w_lay_out_wo_f(x):
            """ Minimize sum of square of relative differences, for u, v, f, lay, and lay_f
            """
            v, u, f, lay, lay_f = unravel_sut(x)
            delta_v = np.sum(((self.v0.values - v) * core.one_over(self.v0)) ** 2)
            delta_u = np.sum(((self.u0.values - u) * core.one_over(self.u0)) ** 2)
            delta_lay = np.sum(((self.lay0.subst.values - lay) * core.one_over(self.lay0.subst.values)) ** 2)
            return delta_v + delta_u + delta_lay

        # Defining constraints as nested functions
        # --------------------------------------------------------------------

        def const_physical(x):
            """ Conservation of all layers in process inputs and outputs"""
            #
            # TODO: missing lay.net_goods in this picture
            #
            vmu, f, lay, lay_f = unravel_var(x)
            if len(f):
                out = list(self.lay0.cons @ lay @ vmu + self.lay_f0.cons @ lay_f @ f)
            else:
                out = list(self.lay0.cons @ lay @ vmu)
            return out

        def const_lay_minmax(x):
            """ apply lay_max constraint"""
            out = []
            __, __, lay, __ = unravel_var(x)
            out += list(np.ravel(lay - self.lay_min))
            out += list(np.ravel(self.lay_max - lay))
            return out

        def const_lay_f_minmax(x):
            """ apply lay_max constraint"""
            out = []
            __, __, __, lay_f = unravel_var(x)
            out += list(np.ravel(lay_f - self.lay_f_min))
            out += list(np.ravel(self.lay_f_max - lay_f))
            return out

        def const_lay_bound_minmax(x):
            """ Make sure product composition layers are internally consistent"""
            out = []
            __, __, lay, __ = unravel_var(x)
            comp = self.lay.cons @ lay
            out += list(np.ravel(comp.values - self.lay_bound.values))
            out += list(np.ravel(self.lay_bound.values - comp.values))
            return out

        def const_lay_f_bound_minmax(x):
            """ Make sure product composition layers are internally consistent"""
            out = []
            __, __, __, lay_f = unravel_var(x)
            comp = self.lay_f.cons @ lay_f
            out += list(np.ravel(comp.values - self.lay_f_bound_min.values))
            out += list(np.ravel(self.lay_f_bound_max.values - comp.values))
            return out

        def const_tc_subst_overall(x):
            """Apply upper and lower bounds to transfer coefficients"""
            out = []
            v, u, f, lay, lay_f = unravel_sut(x)
            for i in range(lay.shape[0]):
                out += list(np.ravel(
                    self.tc_max[i, :] - (lay[i, :] * v / lay[i, :].dot(u))))
                out += list(np.ravel(
                    (lay[i, :] * v / lay[i, :].dot(u)) - self.tc_min[i, :]))
            return out

        def const_tc_el_overall(x):
            raise NotImplementedError()

        def const_tc_subst_perinput(x):
            # TODO: Needs fixing
            raise NotImplementedError("""A bound on TC_subst must take into
            account net_subst(), which depends on both lay.goods _and_
            lay.subst""")
            out = []
            v, u, f, lay, lay_f = unravel_sut(x)
            for i in range(lay.shape[0]):
                loc = self.lay.subst.index[i]
                out += list(
                    (lay[i, :] * u) @ (self.tc_max.loc[loc]) - lay[i, :] * v)
                out += list(
                    (lay[i, :] * v - lay[i, :] * u) @ (self.tc_min.loc[loc]))
            return out

        def const_tc_el_perinput(x):
            # TODO: Needs fixing
            #
            raise NotImplementedError("""A bound on TC_subst must take into
            account net_subst(), which depends on both lay.goods _and_
            lay.subst""")
            #
            #
            out = []
            v, u, f, lay, lay_f = unravel_sut(x)
            for i in range(self.lay.con.shape[0]):
                loc = self.lay.cons.index[i]
                cons_lay = self.lay.cons.loc[i, :] @ lay
                out += list((cons_lay * u) @ self.tc_max.loc[loc] - cons_lay * v)
                out += list(cons_lay * v - (cons_lay * u) @ self.tc_min.loc[loc])
            return out

        def const_flow_bounds_and_reversing(x):
            """ Apply upper and lower bounds to flows, ensure no reverse in
            direction, and no new flows"""
            # TODO: do the same with f?
            v, u, __, __, __ = unravel_sut(x)

            out = list(np.ravel(self.v_max.values - v))
            out += list(np.ravel(v - self.v_min.values))

            out += list(np.ravel(self.u_max.values - u))
            out += list(np.ravel(u - self.u_min.values))
            return out

        # Putting together the constraints
        # ----------------------------------------------------------------------

        #if self.tc_max is not None:
        #    if isinstance(self.tc_max.index, pd.MultiIndex):
        #        if self._all_are_conservative(
        #                self.tc_max.index.get_level_values(0)):
        #            cons += [{'type': 'ineq', 'fun': const_tc_el_perinput}]
        #            print("TC apply to conservative properties per inputs")
        #        else:
        #            cons += [{'type': 'ineq', 'fun': const_tc_subst_perinput}]
        #            print("TC apply to substances per input")
        #    else:
        #        if self._all_are_conservative(self.tc_max.index):
        #            cons += [{'type': 'ineq', 'fun': const_tc_el_overall}]
        #            print("TC apply to conservative properties overall")
        #        else:
        #            cons += [{'type': 'ineq', 'fun': const_tc_subst_overall}]
        #            print("TC apply to substances overall")





                    # if set(tc_max.index.get_level('property')) subset subst:
                    #   cons += constraint on substances
                    # elif set(tc_max.index.get-level('property')).issubset(cons_prop)
                    #   cons += constrain on elements
                    # else:
                    #   ValueError("Seem to be mixing elements and substances in TC")

        # The actual optimization
        # ----------------------------------------------------------------------
        # TODO: find a good way to ensure all dataframes are similarly reindexed()
        #
        x0 = ravel_var(self.v0, self.u0, self.f0, self.lay0.subst, self.lay_f0.subst)
        if self.f0 is not None:
            cons = [{'type': 'eq', 'fun': const_physical},
                    {'type': 'ineq', 'fun': const_lay_minmax},
                    {'type': 'ineq', 'fun': const_lay_f_minmax},
                    {'type': 'ineq', 'fun': const_lay_bound_minmax},
                    {'type': 'ineq', 'fun': const_lay_f_bound_minmax},
                    {'type': 'ineq', 'fun': const_flow_bounds_and_reversing},
                    ]
            sol = minimize(objective_func_w_lay_out, x0, constraints=cons, options={'maxiter': 500, 'disp': True})
        else:
            cons = [{'type': 'eq', 'fun': const_physical},
                    {'type': 'ineq', 'fun': const_lay_minmax},
                    {'type': 'ineq', 'fun': const_lay_bound_minmax},
                    {'type': 'ineq', 'fun': const_flow_bounds_and_reversing},
                    ]
            sol = minimize(objective_func_w_lay_out_wo_f, x0, constraints=cons, options={'maxiter': 500, 'disp': True})

        # adjust lay(outputs) and tech, to
        # minimize square of relative diff of flows and compositions
        #
        # s.t.:
        #
        # sum(tech * lay) == 0  [conservation of eacy layer]
        #
        # lay < lay_max
        # lay < lay_min
        #
        # u_min < u < u_max
        # v_min < v < v_max
        #
        # tech_i * lay_i / sum_inputs(tech * lay) > tc_min_i
        # tech_i * lay_i / sum_inputs(tech * lay) < tc_min_i
        # set self.lay_outputs

        # The result

        self.v, self.u, self.f, self.lay.subst, self.lay_f.subst = unravel_sut(sol['x'], with_labels=True)

        if sol['success']:
            print("It Worked!")
        else:
            print(sol['message'])


    def make_bounds(self, r, strict=True, r0=None, strict0=False, name=None):
        """
        A function that defines upper and lower bounds starting from an initial
        vector

        Args
        ----
        :param r: Variable from which boundaries are defined
        :param strict: If true, treat defined value in r as both min and max value
                       Otherwise [default]:
                            r[i] > 0  leads to bound [min:0, max:nan]
                            r[i] < 0 leads to bounds [min:nan, max:0]
                            r[i] == 0 lead to bounds[min:0, max:0]
        :param norm: If true, bounds must range between 0 and 1
                     [default False]
        :param r0: Optional, second variable from which boundaries are set.
                   If defined, the strictest boundary between r and r0 is selected.
                   N.B. if strict==True, only applies to bounds defined by r, not r0

        :return: lower, upper
        """


        def bound_definition(an_r, is_strict):
            up = copy.deepcopy(an_r)
            low = copy.deepcopy(an_r)
            if not is_strict:
                # Bound "inf" if value is strictly positive, "-inf" if strictly negative, all
                # against 0 as the other bound
                # True zero values must remain zero
                up[up > 0] = np.nan
                up[up < 0] = 0.0
                low[low < 0] = np.nan
                low[low > 0] = 0.0
            return low, up


        if core.pd_isnull(r0):
            if core.pd_isnull(r):
                #
                # Essentially got passed empty dataframes or Nones, nothing to do, return in kind
                lower, upper = r, r
            else:
                # apply only with r
                lower, upper = bound_definition(r, strict)
        else:
            if core.pd_isnull(r):
                # Apply using r0
                lower, upper = bound_definition(r0, strict0)
            else:
                # both defined, apply using both
                lower, upper = bound_definition(r, strict)
                lower0, upper0 = bound_definition(r0, strict0)

                # pick the stricter (larger) lower bound:
                bo = lower.isna()
                lower[bo] = lower0[bo]
                bo = lower <= lower0
                lower[bo] = lower0[bo]

                # pick the stricter (smaller) upper bound
                bo = upper.isna()
                upper[bo] = upper0[bo]
                bo = upper >= upper0
                upper[bo] = upper0[bo]

        # check that these sets of constraints do not lead to infeasible solutions:
        if np.any(core.gt_significantly(lower, upper)) or np.any(core.lt_significantly(upper, lower)):
            raise ValueError(""" Warning! Infeasible situation for {}: a min
            boundary is higher than a max boundary.

            That can happen if make_bounds accepts both r and r0 arguments and
            optional argument 'strict' == True.  Specifically if (r[i] != r0[i]
            != NaN) and (strict0==True) for any i.
            """.format(name))

        return lower, upper


    def check_balance(self, init, rtol=1e-05, atol=1e-08):
        """ A simple function to check if conserved properties are conserved

        Args
        ----
         * init: True to check the initial system (u0, v0, ...), false to check new system (u, v, f, lay,...)
         * rtol, atol: relative and absolute tolerance for the system to be considered balanced

        Returns
        -------
         * imbalance:
             * if all fine: None
             * if lay or lay_f insane: tuple of booleans, diagnostics
             * if flows out of balance: dataframe with detailed breakdown

        Depends
        --------
         * _check_sanity_lay()

        """

        def calc_balances(u, v, f, lay_net_cons, lay_f_net_cons, l_pro, l_str):
            # TODO - gmb : need to decide whether self.lay is allowed to have more
            # commodities than is present in sut vectors... For now, replace:
            #
            # inputs = lay_net_cons @ u
            # outputs = lay_net_cons @ v
            # by:
            try:
                inputs = lay_net_cons[u.index] @ u.fillna(0.)
                outputs = lay_net_cons[u.index] @ v.fillna(0.)
            except TypeError:
                raise TypeError("Could the 'u' or 'v' vectors contain non-numeric values?")


            # Default values
            imbalances = None
            factor_outputs = pd.Series(index=outputs.index).fillna(0.0)

            # Calculated factor outputs
            if not core.pd_isnull(f):
                factor_outputs = lay_f_net_cons[f.index] @ f.fillna(0.)

            is_balanced = np.allclose(inputs, outputs + factor_outputs, rtol, atol)
            if is_balanced:
                print("OK. The system is balanced")
            else:
                delta = inputs - outputs - factor_outputs
                imbalances = pd.concat([inputs, outputs, -1 * factor_outputs, delta], axis=1)
                imbalances.columns = ['inputs', 'outputs', 'factor input', 'imbalance']
                print("Careful. The system is NOT balanced.")

            return imbalances

        if init:
            return calc_balances(self.u0, self.v0, self.f0, self.lay0.net_cons(), self.lay_f0.net_cons(), self.l_pro, self.l_str)
        else:
            return calc_balances(self.u, self.v, self.f, self.lay.net_cons(), self.lay_f.net_cons(), self.l_pro, self.l_str)

    def harmonize_dimensions(self):
        def reindex_all(dfs, index=None, columns=None):
            out = []
            for x in dfs:
                if x is not None:
                    if index is not None:
                        x = x.reindex(index=index)
                    if columns is not None:
                        x = x.reindex(columns=columns)
                out += [x]
            return out

        """
        Very much a work in progress! Hic sunt dracones...

        """

        [self.u0, self.v0, self.u, self.v, self.lay0.goods] = reindex_all(
                    [self.u0, self.v0, self.u, self.v, self.lay0.goods], index=self.l_pro.index)

        [self.lay0.goods, self.lay0.subst] = reindex_all([self.lay0.goods, self.lay0.subst], columns=self.l_pro.index)

        [self.f0, self.f] = reindex_all([self.f0, self.f], self.l_str.index)

        [self.lay_f0.subst] = reindex_all([self.lay_f0.subst], columns=self.l_str.index)

        self.lay.reindex_like(self.lay0)
        self.lay_f.reindex_like(self.lay_f0)




    # Applying transfer coefficients
    # --------------------------------------------------------------------

#    def calc_transfer_coefficients(self, to_init, to_cons=True):
#        """ Calculate transfer coefficients
#
#        Args
#        ----
#        to_init: if true, apply to initial system (u0, v0, lay0, etc.)
#        to_cons: if true, apply to conservative property (self.lay.cons),
#                 otherwise other properties (self.lay.subst)
#
#        Returns
#        -------
#        tc: to-product-transfer coefficients
#        tc_f: to-emissions-transfer coefficients
#
#        Dependencies
#        ------------
#        For more custom use, use calc_tc function in the module
#
#        """
#
#        if to_init:
#            lay = self.lay0
#            lay_f = self.lay_f0
#        else:
#            lay = self.lay
#            lay_f = self.lay_f
#
#        if to_cons:
#            tc, tc_f = calc_tc(self.u0, self.v0, self.f0,
#                               lay.net_cons(), lay_f.net_cons())
#        else:
#            tc, tc_f = calc_tc(self.u0, self.v0, self.f0,
#                               lay.net_subst(), lay_f.net_subst())
#
#        return tc, tc_f


    def calc_transfer_coefficients(self, data):
        """
        A function that transforms the raw data in three tc matrices for each
        mass level (goods, substances and conservatives).

        Args
        ----
        data: datataframe containing all the tc for the process in the format:
            ['cons','subst','goods','goods_out','value']

        Returns
        ----
        tc_goods: dataframe
            Matrices of tc on the goods level
        tc_subst: dataframe
            Matrices of tc on the substances level
        tc_cons: dataframe
            Matrices of tc on the conservatives level

        """
        tc_data = pd.DataFrame(columns = self.__tc_columns)
        tc_data = tc_data.append(data,ignore_index=True,sort=False)

        #Initialize the three tc dataframe
        tc_goods = pd.DataFrame()
        tc_subst = pd.DataFrame()
        tc_cons = pd.DataFrame()

        #Check for dependance in data
        dependency_check = 3 - tc_data[['cons','subst','goods']].isnull().sum(axis=1)

        if all(dependency_check == 1): # No dependency involved
            for i in tc_data.index:

                # TC on goods to goods
                if tc_data.isnull().loc[i,'goods'] == False: #is this condition enough?
                    tc_goods = pd.concat([tc_goods, pd.DataFrame(tc_data.loc[i,'value'],
                                                 index = [tc_data.loc[i,'goods']],
                                                 columns = [tc_data.loc[i,'goods_out']])],axis=0,sort=True)

                # TC on substances to goods
                elif tc_data.isnull().loc[i,'subst'] == False:
                    tc_subst = pd.concat([tc_subst,pd.DataFrame(tc_data.loc[i,'value'],
                                                 index = [tc_data.loc[i,'subst']],
                                                 columns = [tc_data.loc[i,'goods_out']])],axis=0,sort=True)

                # TC on conservative layer to goods
                elif tc_data.isnull().loc[i,'cons'] == False:
                    tc_cons = pd.concat([tc_cons,pd.DataFrame(tc_data.loc[i,'value'],
                                                 index = [tc_data.loc[i,'cons']],
                                                 columns = [tc_data.loc[i,'goods_out']])],axis=0,sort=True)

            # If duplicates, sum them
            self.tc_goods = tc_goods.groupby(tc_goods.index).sum()
            self.tc_subst = tc_subst.groupby(tc_subst.index).sum()
            self.tc_cons = tc_cons.groupby(tc_cons.index).sum()

        else: #dependency involved, to be continued
            print('dependency involved, not developed yet')
            self.tc_goods = []
            self.tc_subst = []
            self.tc_cons = []
        return self.tc_goods,self.tc_subst,self.tc_cons;

    def calc_normalized_output_composition(self, goods_out, subst_out, cons_out, label, lay):

        # default
        u = pd.Series()
        v = pd.Series()
        f = pd.Series()

        net_outs = self.calc_net_outputs(cons_out, subst_out, goods_out, lay)
        lays = [lay.l_cons, lay.l_subst, lay.l_goods]


        # across all three net_*_outs, which outputs are actually non zero, on any layer
        has_output = pd.Series(index=label.index)
        for nout in net_outs:
            has_output |= nout.sum() != 0
        has_output

        # Initialize normalization vector
        tmp_norm = pd.Series(index=has_output[has_output].index)
        norm = tmp_norm.copy(deep=True)

        # Try to normalize using reference property
        for i, l in enumerate(lays):  # N.B. Built as a foreloop for generality, but typically reference property will be in l_cons
            nout = net_outs[i]

            # indexes of pertinent layers
            ix_lay = set(l.index) & set(nout.index)

            # Dict to translate reference_property name into the index of this property
            d = dict(zip(l.loc[ix_lay, 'name'], l.loc[ix_lay].index))

            # Matching: for each output flow (label.index), the layer index of its reference property
            ref_matches = label.loc[has_output].reference_property.map(d).dropna()

            # Find values to normalize by
            tmp_norm.loc[ref_matches.index] = nout.lookup(ref_matches, ref_matches.index)  # TODO: looping summation assumes NO common name for properties across layers (e.g., a product named "mass" and a substance named "mass")
            norm = norm.fillna(tmp_norm)

            if not np.any(norm.isna()):
                # We are done, no more looping
                break

        # Otherwise normalize relative to any common property
        ix_unmatched = list(norm.loc[norm.fillna(0.0) == 0].index)

        if ix_unmatched:
            net_lays = [lay.net_cons(), lay.net_subst(), lay.net_goods()]

        for i in ix_unmatched:
            for nlay, nout in zip(net_lays, net_outs):
                # if there is a net output on the current layer for flow i
                if len(nout) and np.any(nout[i]):
                    # Find a common property between net output and flow composition
                    prop_out = nout.loc[nout[i] != 0, i].index
                    prop_lay = nlay.loc[nlay[i] != 0, i].index
                    prop_common = set(prop_out) & set(prop_lay)
                    if prop_common:
                        # Calculate normalization factor for dividing net output
                        prop = prop_common.pop()
                        norm[i] = nout.loc[prop, i] / nlay.loc[prop, i]

                        break  # it worked, move on to the next ix_unmatched

        # Re-evaluate whether we missed any    
        ix_unmatched = list(norm.loc[norm.fillna(0.) == 0].index)
        if ix_unmatched:
            raise ValueError("Could not identify a normalization factor for these flows: {}".format(ix_unmatched))
        #else:
        #    norm = norm.reindex(label.index).fillna(0)


        norm_goods_out = goods_out * core.one_over(norm)
        norm_subst_out = subst_out * core.one_over(norm)

        return norm, norm_goods_out, norm_subst_out



    def apply_transfer_coefficients(self, convention=0): # that should work... to be tested
        """
        A function that calculates the process supplies base on the tc for every
        different situations.

        Args
        ----
        tc_goods,tc_subst,tc_cons: dataframe
            Transfer coefficients matrices defines by create_tc_matrix

        Returns
        ----
        inputs: dataframe
            Mass flows of the process inputs
        outputs: dataframe
            Mass flows of the process outputs

        """
        # TODO: sign convention should be defined elswhere probably!
        #Change signs by 0
        u1 = self.u0.replace(['-','+'] , 0.)
        v1 = self.v0.replace(['-','+'] , 0.)
        f1 = self.f0.replace(['-','+'] , 0.)

        tc_check = (self.tc_goods.size >0,self.tc_subst.size>0,self.tc_cons.size>0)

        options = pd.DataFrame([
                 [True,False,False],
                 [False,True,False],
                 [False,False,True],
                 [True,True,False],
                 [True,False,True],
                 [False,True,True],
                 [False,False,False],
                 [True,True,True]],
                 index= ['TC1','TC1','TC1','TC2','TC3','TC3','impossible','TC4'] , columns = ['goods','subst','cons'])
        options = options.reset_index()

        tc_type = [options.loc[i]['index'] for i in options.index if list(options[['goods','subst','cons']].loc[i]) == list(tc_check)][0]

        if tc_type == 'impossible':  #No transfert coefficients are defines by the user
            raise ValueError('System is underspecified. Transfer coefficients need to be define')
        elif tc_type == 'TC1' : #Transfer coefficients are defined on only one level
            inputs, outputs = self._apply_tc1()
        elif tc_type =='TC2':
            if set(self.tc_goods.columns).isdisjoint(self.tc_subst.columns) == True :
                inputs, outputs = self._apply_tc1()
            else:
                self._apply_tc2()
        elif tc_type =='TC3':
            self._apply_tc3()
        elif tc_type =='TC4':
            self._apply_tc4()

        return inputs, outputs;



    # ==========================================================================
    """ Internal Methods """

    # ==========================================================================


    # Transfer coefficients internal methods
    # --------------------------------------------------------------------
    def _convert_supply_to_flow(self, u, v, f, convention):
        """ A function convert process supplies in a process inputs and outputs.

        Args
        ----
        u: series
            TODO
        v: series:
            TODO
        f: series:
            Emissions
        convention: 0 or 1
                    if 0, convention (Guillaume)
                    if 1, convention (l'autre)

        Returns
        ----
        inputs: dataframe
            Mass flows of the process inputs
        outputs: dataframe
            Mass flows of the process outputs

        """
        #
        if convention == 0 :
            #Deal with f0 first
            inputs = abs(f[f<0])
            outputs = f[f>0]

            #Deal with u0
            cat_in_u = self.lay.net_goods()[u[u !=0].index].loc[:,self.lay.net_goods()[u[u !=0].index].sum() >= 0].columns
            cat_out_u = self.lay.net_goods()[u[u !=0].index].loc[:,self.lay.net_goods()[u[u !=0].index].sum() < 0].columns
            inputs = pd.concat([inputs,u[cat_in_u]])
            outputs = pd.concat([outputs,abs(u[cat_out_u])])

            #Deal with v0
            # TODO - gmb: not only depend on net_goods, net_subst also pertinent
            cat_out_v  = self.lay.net_goods()[v[v !=0].index].loc[:,self.lay.net_goods()[v[v !=0].index].sum() >= 0].columns
            cat_in_v  = self.lay.net_goods()[v[v !=0].index].loc[:,self.lay.net_goods()[v[v !=0].index].sum() < 0].columns
            inputs = pd.concat([inputs,abs(v[cat_in_v])])
            outputs = pd.concat([outputs,abs(v[cat_out_v])])

        elif convention == 1: #not that sure I have the good convention
            #Deal with f0 first
            inputs = f[f<0]
            outputs = f[f>0]

            #Deal with u0
            inputs = pd.concat([inputs,u[u>0]])
            outputs = pd.concat([outputs,u[u<0]])

            #Deal with v0
            inputs = pd.concat([inputs,v[v<0]])
            outputs = pd.concat([outputs,v[v>0]])

        else:
            raise ValueError('This convention does not exist. Choose between 0 or 1.')

        return inputs, outputs;

    def _convert_flow_to_sut(self, inputs=None, outputs=None, lay_goods=None, lay_subst=None, convention='reverse_flows', how_lay='replace'):
        """ A function convert process inputs and outputs in process supplies.

        Args
        ----
        inputs: dataframe
            Mass flows of the process inputs
        outputs: dataframe
            Mass flows of the process outputs
        convention: 0 or 1
                    if 0, convention (Guillaume)
                    if 1, convention (l'autre)

        Returns
        ----
        u: series
        v: series

        """
        u = self.u0.copy(deep=True)
        v = self.v0.copy(deep=True)

        # TODO: This is not robust against a value being non-null in _both_ u and v
        #       Note: for now we don't distinguish between '+' or '-'. there just needs to be a non-null entry.

        # TODO: null outputs of emissions can end up with a negative sign as if they were resources
        # e.g., f['CO2 [kg]'] = -0.0



        def add_layer(ix, laysign=1.0):
            try:
                self.lay.add_layer_data(goods=laysign * lay_goods[[ix]], how=how_lay)
            except TypeError:
                pass

            try:
                self.lay.add_layer_data(subst=laysign * lay_subst[[ix]], how=how_lay)
            except TypeError:
                pass


        if convention == 'reverse_flows':
            sign = -1.0
            laysign = 1.0
        elif convention == 'reverse_lay':
            sign = 1.0
            laysign = -1
        else:
            raise ValueError("There is no such convention as {}.".format(convention))

        if inputs is not None:
            for i in inputs[inputs != 0].index:
                #adjust the value of u if its defines in u0
                if i in u.index and core.notnull(self.u0[i]):
                    u[i] = inputs[i]
                    add_layer(i)
                # if the inputs is in v0, adjust its value in v
                elif i in v.index and core.notnull(self.v0[i]):
                    v[i] = sign * inputs[i]
                    add_layer(i, laysign)
                # if the inputs is an emission, it is always negative
                else:
                    raise ValueError("Could not identify whether {} belongs in supply or in use".format(i))

        if outputs is not None:
            #Work on non-zero outputs
            for i in outputs[outputs !=0].index:
                if i in v.index and core.notnull(self.v0[i]):
                    v[i] = outputs[i]
                    add_layer(i)

                elif i in u.index and core.notnull(self.u0[i]):
                    #Adjust the sign of u depending of the user inputs in u0
                    u[i] = sign * outputs[i]
                    add_layer(i, laysign)
                else:
                    raise ValueError("Could not identify whether {} belongs in supply or in use".format(i))

        return u, v


    def apply_tc(self, init=True): 
        """

        """
        self.harmonize_dimensions()

        if init:
            u = self.u0
            v = self.v0
            f = self.f0
            lay = self.lay0
            lay_f = self.lay_f0
        else:
            u = self.u
            v = self.v
            f = self.f
            lay = self.lay
            lay_f = self.lay_f

        ins, ins_f, outs, outs_f = self.calc_inputs_and_outputs(u, v, f, lay, lay_f)

        goods_out, subst_out, cons_out = self._apply_tc_sequentially(ins, ins_f, lay, lay_f, strict=False)

        goods2goods, goods2fp = self._split_flows_togoods_and_tofactors(goods_out)
        subst2goods, subst2fp = self._split_flows_togoods_and_tofactors(subst_out)
        cons2goods, cons2fp = self._split_flows_togoods_and_tofactors(cons_out)

        # Divide flow into flows of goods and compositions of goods
        norm, norm_goods_out, norm_subst_out = self.calc_normalized_output_composition(goods2goods, subst2goods,
                cons2goods, self.l_pro, lay)

        # Organize in a supply and Use
        new_u, new_v = self._convert_flow_to_sut(outputs=norm, lay_goods=norm_goods_out, lay_subst=norm_subst_out, how_lay='replace')
        new_u = core.dropnull(new_u)
        new_v = core.dropnull(new_v)
        self.u[new_u.index] = new_u
        self.v[new_v.index] = new_v
        self.lay.add_layer_data(subst=norm_subst_out.dropna(how='all', axis=1), 
                                goods=norm_goods_out.dropna(how='all', axis=1),
                                how='replace')

        # Divide flows into flows of factors of production and compositions of factors of production
        f, _, norm_subst_out = self.calc_normalized_output_composition(goods2fp, subst2fp, cons2fp, self.l_str, lay_f)
        f = core.dropnull(f)
        self.f[f.index] = f
        self.lay_f.add_layer_data(subst=norm_subst_out, how='replace')

    def _apply_tc_sequentially(self, inputs, inputs_f, lay, lay_f, strict=True):
        """
        Args
        ----
        inputs : Pandas Series [goods]
            Vector of goods input

        inputs_f : Pandas Series [factors]
            Vector of factor inputs

        lay, lay_f : Layer objects


        Dependencies
        ------------
        self.tc_goods, self.tc_subst, self.tc_cons


        Returns
        -------
        goods_out : Pandas DataFrame [goods x goods]
            In which good does each good go?

        subst_out : Pandas Dataframe [subst x goods-and-factors]
            In which good or emission/factor does each remaining substance (not transferred as part of goods_out) go?

        cons_out : Pandas DataFrame [cons x goods-and-factors]
            In which good or emission/factor does each remaining conservative matter go (not transferred as part of
            goods_out or subst_out)?
        """
        # Reflection: could make it a static function if we pass tc coefficients as arguments



        # Default values
        goods_out = pd.DataFrame()
        subst_out = pd.DataFrame()
        cons_out = pd.DataFrame()

        # Calculate destination of each good
        # For now, we either have the top good, or the "final" (most detailed) good
        # TODO: Will want to have  a more looping approach, where we give precedence on top goods, then tier1, then tier2,
        # Will want to do this recursively, actually
        # etc.
        if len(self.tc_goods):
            # Transfer top goods
            goods_out, remaining_inputs = tcmul(self.tc_goods, inputs)
            if np.any(remaining_inputs) and np.any(lay.goods):
                # Transfer final goods
                other_goods_out, remaining_inputs = tcmul(self.tc_goods, core.dot(lay.net_goods(), remaining_inputs))
                goods_out += other_goods_out
        else:
            remaining_inputs = inputs

        # Of physical substances in remaining goods inputs, determine in which good or substance it gets embedded
        # TODO: probably crashes if tc_subst defined but not lay.subst -- 
        if np.any(lay.subst):
            subst_in = (core.dot(lay.net_substp(), remaining_inputs)).add(core.dot(lay_f.net_subst(), inputs_f), fill_value=0.)
            if len(self.tc_subst):
                subst_out, remaining_subst_in = tcmul(self.tc_subst, subst_in)
            else:
                remaining_subst_in = subst_in

        # Of physical conserved properties in remaining substance inputs, determine in which good or substance it gets embedded
        if np.any(lay.cons):
            cons_in = core.dot(lay.cons.fillna(0.), remaining_subst_in)
            if len(self.tc_cons):
                cons_out, remaining_cons = tcmul(self.tc_cons, cons_in)
            else:
                remaining_cons = cons_in

        if strict:
            ix_remaining = set(remaining_cons.loc[remaining_cons > 0 ].index) - {'Total mass [kg]'}
            if ix_remaining:
                raise ValueError("Not all inputs have a transfer coefficient. Remaining cons: {}".format(ix_remaining))

        return goods_out, subst_out, cons_out

    def _apply_tc2():
        """
        A function that apply the TC to the process. To be use when a transfer coefficient
        defines a good on two levels (goods and substances).

        Args
        ----
        :

        Returns
        ----
        :

        """
        print('TC2 method should be applied, but is not yet developed')
        return;


    def _apply_tc3():
        """
        A function that apply the TC to the process. To be use when tc are defined on two levels
        including the conservative layer.

        Args
        ----
        :

        Returns
        ----
        :

        """
        print('TC3 method should be applied, but is not yet developed')
        return;

    def _apply_tc4():
        """
        A function that apply the TC to the process when tc are defined on 3 levels.
        Combination of tc1 and tc3

        Args
        ----
        :

        Returns
        ----
        :

        """
        print('TC4 method should be applied, but is not yet developed')
        return;

    # Other internal methods
    # --------------------------------------------------------------------

    def _all_are_conservative(self, a):
        if set(a) - set(self.lay.cons.index):
            return False
        else:
            return True



    def _get_ref_properties(self, x):

        # remove prefix, if any
        ix_pure = [re.sub('^other ', '', i) for i in x.index]

        ref_subst = [i for i in set(x.index) if re.sub('^other ', '', i) in self.l_pro.reference_property]
        ref_prop = [i for i in set(self.l_pro.reference_property) if i in ix_pure]

        return ref_prop, ref_subst


    def _concat_lay(self, old, new, nan2zero=True):

        # Get datatype issues out of the way
        new = new.astype(float)

        # Default: do nothing
        out = old

        # Initial tests
        new_layer_types = set(new.index) - set(old.index)
        redundant = set(old.columns) & set(new.columns)

        # Refuse new layer types
        if new_layer_types:
            print("Warning! The following layers are not yet defined: {}."
                  " Please first define these by the 'build_conserved_property_layer()' method.".format(new_layer_types))

        # Refuse redundancy in columns
        elif redundant:
            print("Warning, these columns already exist: {}".format(redundant))

        # All fine, concat
        else:
            out = pd.concat((old, new), axis=1, sort=False)
            if nan2zero:
                out = out.fillna(0.0)
        return out


    def _define_lay_bounds(self, lay, label):

        # TODO: eventually, the label should be part of lay

        # define empty
        bound = pd.DataFrame(index=lay.cons.index, columns=lay.subst.columns)
        bo_bound = bound.copy()

        # Get reference property for  every layer, in order
        refs = [label.loc[i, 'reference_property'] for i in bound.columns]

        # Conservative property and reference property intersections
        for i in bo_bound.index:
            bo_bound.loc[i, :] = [i == j for j in refs]

        # Define constraint on reference properties
        bound[bo_bound] = 1.0

        return bound


    def _split_flows_togoods_and_tofactors(self, df, row_labels=None):
        """ Split flow tables between those that become embedded in goods and those embedded in factors
        """
        if row_labels is None:
            ix = None
        else:
            ix = row_labels.index

        to_goods = df.reindex(columns=self.l_pro.index, index=ix, fill_value=0)
        to_factors = df.reindex(columns=self.l_str.index, index=ix, fill_value=0)
        return to_goods, to_factors
    def add_layer_types(self, new):
        raise DeprecationWarning("add_layer_types replaced by build_conserved_property_layer")

def calc_tc(u, v, f, lay, lay_f):
    """ Calculate overall transfer coefficients """

    # Distinguish between inputs and outputs from nature based on sign
    f_in = f.copy()
    f_in[f > 0] = 0
    f_out = f.copy()
    f_out[f < 0] = 0

    # Total inputs of whatever lay[_f] is
    in_tot = lay @ u - lay_f @ f_in

    # Output flows in products divided by total inputs
    tc_v = (lay * v).divide(in_tot, axis='index')

    # Output flows in stressors (emissions) divided by total inputs
    tc_f = (lay_f * f_out).divide(in_tot, axis='index')

    return tc_v, tc_f


def calc_composition(lay_good, lay_subst=None, lay_cons=None):
    """ Calculate composition, in good, substance, or conservative layer

    Depending on the arguments provided, calculate one composition or the
    other

    """
    # Define Identity matrix
    I = core.eye_like(lay_good)

    # Calculate goods cummulatively integrated _inside_ each good:
    #     comp_good = lay_good + lay_good**2 + lay_good**3 + ...
    # which is equivalent to:
    #     comp_good = (I - lay_good)**-1 - I
    comp_good = np.linalg.inv(I - lay_good) - I
    out = comp_good

    if lay_subst is not None:
        # If lay_subst provided, calculate substance composition
        # comp_subst = direct_subst + subst_in_embodied_goods
        comp_subst = lay_subst + lay_subst @ comp_good
        out = comp_subst

        if lay_cons is not None:
            # If lay_cons provided, calculate conservative (mass, element)
            # composition
            comp_cons = lay_cons @ comp_subst
            out = comp_cons

    return out

def tcmul(tc, x):
    """ Calculates the outputs from a tc and a vector of inputs; and identifies the remaining (non-transferred)
    inputs, i.e., inputs for which there is no matching tc coeffcients
    """
    outs = tc.multiply(x[tc.index], axis='index').fillna(0.)
    remaining = x.sub(outs.sum(1), fill_value=0)
    return outs, remaining
