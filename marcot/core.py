import re
import warnings
import numpy as np
import pandas as pd
from pandas.testing import assert_frame_equal
import pdb

def abbreviate(a):
    """ Removes null rows and columns"""

    # Remove null rows first
    a = a.loc[a.sum(1) != 0.0, :]

    # then remove null columns
    a = a.loc[:, a.sum(0) != 0.0]

    return a

def insert_values_except_nans(df, dfins):

    msg = ''

    # Create a dataframe identical to dfins except with its NaNs replace by entries from df
    ins = df.reindex_like(dfins).where(dfins.isna(), other=dfins)

    # Insert this dataframe
    df = df.copy()
    try:
        df.loc[ins.index, ins.columns] = ins
    except KeyError as err:
        msg = str(err)

    if msg:
        raise KeyError("Some dimensions in inserted property layer not found in original layer. Not allowed with 'update_*' option." + msg)

    return df


def eye_like(df):
    """ Generate an identity matrix similar to a dataframe"""
    I = np.eye(df.shape[0], df.shape[1])
    I = pd.DataFrame(I, index=df.index, columns=df.index)
    return I

def df_inv(df):
    """ Perform inverse, and restore DataFrame indexes once done"""
    return pd.DataFrame(np.linalg.inv(df), index=df.index, columns=df.index)


def pd_allclose(a, b):
    """ Test if two Pandas DataFrames or Series are all close

    This function does **not** check the order of the indexes and columns, nor
    does it test for exact equality (rounding errors)

    Parameters
    ----------
    a, b: DataFrames or Series
       Variables to be compared

    Returns
    -------
        True if all coefficients close. False if Dataframes differ, or if
        variables being compared cannot even be converted to a dataframe

    """
    try:
        pd.testing.assert_frame_equal(pd.DataFrame(a), pd.DataFrame(b), check_like=True, check_exact=False)
        return True
    except AssertionError:
        return False
    except ValueError:
        # Not even dataframeable
        return False

def series_allclose(a, b, allow_empty=True, nanisnull=False):

    # If empty series is not allowed, then the prensence of an empty series returns a fals comparison
    if not allow_empty:
        if not len(a) or not len(b):
            return False

    # Preprocess if NaN can equate 0. Reindex all and replace by zero
    if nanisnull:
        ix = a.index.union(b.index)
        a = a.reindex(ix).fillna(0.)
        b = b.reindex(ix).fillna(0.)

    # In any other case, comparison proceeds
    try:
        pd.testing.assert_series_equal(a, b, check_names=False)
        return True
    except AssertionError:
        return False


def one_over(r):
    """ return 1/r except r=0, then return 0 """
    with np.errstate(divide='ignore'):
        one_over_r = np.array([1.0]) / r
    one_over_r[r == 0.0] = 0.0
    return one_over_r


def gt_significantly(a, b, rtol=1e-05, atol=1e-08):
    """ Are all entries of A significantly greater than those of B? """
    out = False

    # if without any close
    if ~ np.any(np.isclose(a, b, rtol=rtol, atol=atol)):
        # and all those significant differences are positive
        if np.all(a > b):
            out = True
    return out


def lt_significantly(a, b, rtol=1e-05, atol=1e-08):
    """ Are all entries of A significantly smaller than those of B? """
    out = False

    # if without any close
    if ~ np.any(np.isclose(a, b, rtol=rtol, atol=atol)):
        # and all those significant differences are positive
        if np.all(a < b):
            out = True
    return out

def pd_isnull(x):

    if x is None:
        out = True
    elif len(x) == 0:
        out = True
    else:
        out = False

    return out

def dropnull(ds):
    """ Drop all NaN, all strings, and all 0.0 entries in a data series, keeping only nonnull numbers """

    ds = pd.to_numeric(ds, errors='coerce').dropna()
    return ds[ds != 0]


def notnull(x):

    if isnan(x) or x == 0:
        out = False
    else:
        out = True

    return out

def isnan(x):
    """ Robust isnan: if not coercible in isnan, then is not NaN"""

    # Default False
    verdict = False

    if x is None:
        verdict = True
    else:
        try :
            verdict = np.isnan(x)

        except TypeError:  # Probably a string
            try:
                verdict = x.casefold() == 'nan'

            except AttributeError: # Maybe not a string afterall, but not nan either
                pass

    return verdict

def flexdot(a, b):


    if pd_isnull(a) or pd_isnull(b):
        if a.ndim == 1 or b.ndim ==1:
            out = pd.Series()
        else:
            out = pd.DataFrame()
    else:
        # Get relevant indexes for an inner product in a and b
        if a.ndim == 2:
            ix_a = set(a.columns)
        elif a.ndim == 1:
            ix_a = set(a.index)
        ix_b = set(b.index)

        # Inner product only affected by common indexes, drop the rest
        if ix_a != ix_b:
            ix = list(ix_a & ix_b)
            if not ix:
                out = pd.DataFrame()
                print("Careful! The two matrices have no indexes in common to perform the dotproduct")
            else:
                out = a[ix].fillna(0.) @ b.loc[ix].fillna(0.)
        else:
            out = a.fillna(0.) @ b.fillna(0.)

    return out

# ----------------------
# aliases
dot = flexdot
