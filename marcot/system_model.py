import copy
import pandas as pd
import numpy as np
try:
    import process_model
    import layers
    import core
except ModuleNotFoundError:
    from marcot import process_model
    from marcot import layers
    from marcot import core
import pdb
from scipy.optimize import linprog


class MarcotModel:
    def __init__(self, generators, y=None, lay0=None,  lay_f0=None, constraints=None):
        """ Define the system to be investigated: resources, technologies,
        demands and constraints

        Args
        ----

        lay_f0 : initial composition of factors of production
                    [lay x str]

        lay0: inital composition of products
                    * Mostly empty, except for old scrap and waste...
                    [lay x pro]

        generators: dictionary of process model objects

        y: final demand

        constraints: additional constraints

        """

        # initial compositions
        # known fixed compositions, typically at the bottom of the pyramid
        self.lay_f0 = lay_f0
        self.lay0 = lay0


        # list of generators
        self.generators = generators

        # final demand
        self.y = y

        # Order in which to introduce processes and product mixes
        #
        # List of tuples:
        #   List length : number of generators
        #   Tuple[0]: next generator to introduce in the product system
        #   tuple[1]: next product mixes that should be generated
        #               (because that product will be used on next round)
        self.hierachy = None

        # Lists of products whose composition is only temporarily defined to enable normalization
        self.initialization = []

        # "foreground" supply and use
        self.U = None
        self.V = None
        self.F_f = None


        # The technology matrix, to be built by marcot
        # rows: multiindex, products id and composition-bin id
        # columns: multiindex, generator id and adapted technology id
        self.A = None

        # The factor and emissions matrix, to be built by marcot
        # rows: multiindex, factors and composition-bin ids
        # columns: multiindex, generator id and adapted technology id
        self.F = None

        # Variable to hold calculated composition best estimates
        # initialized based on initial compositions
        self.lay_f = copy.deepcopy(self.lay_f0)
        self.lay = copy.deepcopy(self.lay0)

        # Remaining heterogeneity in product composition: per technology
        # composition
        #
        # Dimensions: a dictionnary?
        self.lay_heterogeneous = None


        # Characterisation matrix
        self.C = None

        # Background data #TODO: these could be regrouped in a single system
        self.A_bb = None
        self.F_b = None

        # Production mix: what we are solving for
        self.x = None

        # Bounds on x
        self.bounds = None

        # Overall solution
        self.sol = None

    def triangulate(self):
        """ Determine product hierarchy in the economy, from resource to the
        latest computer, to sequentially calculate material compositions and
        their influence on technologies

        Define an ordered sequence of tuples, each containing:
            1) the products that are used for the first time at this level, and
            requiring mixes to be defined
            2) the next technology in the ladder that requires only defined
            products and mixes
        """

        self.hierarchy = None  # Not implemented yet

    def balance_system(self, rtol=1e-3):
        """ The 'main' method: assemble an MFA-consistent LP product system

        Args
        ----
        rtol: relative tolerance of composition mismatches due to byproduct
              composition
        """
        raise NotImplementedError("Place holder")

        # Loop until the compositions converge
        lay_corr = np.zeros_like(lay)
        first = True

        # Loop until convergence
        while first or (lay_corr / lay > rtol):
            first = False

            # Build/rebuild system
            self.build_system()

            # Solve the system, get production volumes
            solve_the_system()

            # Recalculate lay_corr
            lay_corr += lay - (lay_heterogeneous.dot(self.x)) / self.x

    def build_system(self):
        """
        Assemble the A, F, and Lay matrix best we can, following order in self.hierarchy
        """
        # First round of a for-loop
        for h in self.hierarchy:
            gen = self.generators[h]
            combinations = self._find_combinations_of_variants_of_determining_flows(gen)
            # for c in combs
            for cx in combinations.columns:
                # get a valid combination of inputs
                comb = combinations[cx].to_frame('variant')

                # adapt gen to this new mix
                a = _adapt_gen_to_input_variants(gen, comb, self.lay)

                # check which compositions have changed
                change_ix = self._get_new_or_updated_goods(gen)

                # For flows for which we have metadata, update the metadata in case it changed
                # For example temperature
                ix = list(set(change_ix) & set(self.lay.l_goods.index))
                self.lay.l_goods.loc[ix] = gen.lay.l_goods.loc[ix]

                # See if these compositions fit in existing variants or whether to create new ones
                new_variants, variants = self._find_or_define_variants(gen.lay, change_ix, comb)


                # integrate production of new variants in A matrix
                a = a.join(variants).reset_index()
                a['variant'] = a['variant'].fillna(1).astype(int)
                a = a.set_index(['index', 'variant'])

                self.A = pd.concat((self.A, a), axis=1)

                # reindex with variants and integrage new composition in substance layer
                new_subst = gen.lay.subst.T.join(new_variants, how='inner'
                                                 ).reset_index().set_index(['index', 'variant']).T

                # Reindex goods colums with variants
                new_goods = gen.lay.goods.T.join(new_variants, how='inner'
                                                 ).reset_index().set_index(['index', 'variant']).T.dropna(how='all')

                # Reindex goods rows with variants
                new_goods = new_goods.join(variants, how='left')
                new_goods['variant'] = new_goods['variant'].fillna(1).astype(int)
                new_goods = new_goods.reset_index().set_index(['index', 'variant'])

                # Concat new descriptions that are not already defined system-wide
                self.lay.subst = pd.concat((self.lay.subst, new_subst), axis=1)
                self.lay.goods = pd.concat((self.lay.goods, new_goods), axis=1)


        # Remove now useless initialization composition of goods whose composition we just initialized
        if self.initialization:
            self.lay.subst.drop([(td, 1) for td in self.initialization], axis=1, inplace=True)

        # YET TODO
        #    # First, generate product mixes needed for their first use
        #    #
        #    # That is, make sure that there exists a composition that meets the
        #    # minimal requirements of each technology using this product. If
        #    # not, automatically mix with virgin...
        #    try:
        #        mix, comps = generate_product_mix('The right arguments')
        #        append_to_product_system(mix, comps)
        #    except:
        #        print("Mix generation postponed for now. NotImplemented.")


    def _get_new_or_updated_goods(self, gen):
        """ Compare with other/older layer object `lay0` to find goods with new or updated compositions"""

        def find_new_or_updated_goods_compositions(new, org):

            # clean up, remove extraneous columns
            new = new.dropna(how='all', axis=1)

            # Compare across all possible properties
            ix = set(new.index) | set(org.index)

            # reindex to allow for comparison with same dimensions, fillna to avoid NaN == NaN comparisons
            change = ~ (new.reindex(index=ix).fillna(-42) == org.reindex(index=ix, columns=new.columns).fillna(-42)).all()

            # Return list of products that changed according to that specific layer
            changed_goods = change[change].index
            return changed_goods


        change_ix = set()

        # New goods based on new composition data
        change_ix |= set(find_new_or_updated_goods_compositions(gen.lay.subst, gen.lay0.subst))
        change_ix |= set(find_new_or_updated_goods_compositions(gen.lay.goods, gen.lay0.goods))

        change_ix |= set(find_new_or_updated_goods_compositions(gen.lay.l_goods.T, gen.lay0.l_goods.T))

        # Good in gen.u or gen.v that is not in self.A, even if composition is not/hardly defined
        change_ix |= (set(gen.u.dropna().index) | set(gen.v.dropna().index)) - set(self.A.index.get_level_values(0))

        return list(change_ix)

    def finalize_system_init(self):
        """ To be run after defining layers - handles dimensions and multiindexing of layers and SUT"""

        def initialize_system_lay(lay, lay0):
            bo = lay0.subst.any(axis=0)
            lay.subst = lay0.subst.loc(axis=1)[bo]
            lay.subst.columns = pd.MultiIndex.from_product([lay.subst.columns, [1]], names=['id', 'var'])
            lay.cons = copy.deepcopy(lay0.cons)
            return lay

        # Initialize layers
        self.lay = initialize_system_lay(self.lay, self.lay0)
        self.lay_f = initialize_system_lay(self.lay_f, self.lay_f0)

        # initialize system U and V (multiindex with variants)
        self.U = pd.DataFrame(index=self.lay.subst.columns,
                              columns=pd.MultiIndex.from_arrays([[],[]], names=['act', 'var']))
        self.V = pd.DataFrame(index=self.lay.subst.columns,
                              columns=pd.MultiIndex.from_arrays([[],[]], names=['act', 'var']))
        self.F = pd.DataFrame(index=self.lay_f.subst.columns,
                              columns=pd.MultiIndex.from_arrays([[],[]], names=['act', 'var']))

    def _find_or_define_variants(self, lay, change_ix, combs):
        """ For each changed composition, see if the variant already exists, or else create a new  one

        """

        # Initialization
        new_variants = pd.Series(index=change_ix, data=0, name='variant')
        matched_variants = pd.Series(index=change_ix, data=0, name='variant')

        # Compare self.lay and gen.lay pour les
        for good in change_ix:
            good_variants = self.lay.get_variants(good)

            # If variants of this good already exist, see if we are "equal"
            if good_variants:
                for var in good_variants:

                    # Look for a matching variant among substances
                    try:
                        if core.series_allclose(lay.subst[good].dropna(), self.lay.subst[(good, var)].dropna(),
                                                allow_empty=False, nanisnull=True):
                            matched_variants.loc[good] = var
                            print("We have a match for '{}' in substance composition: variant {}".format(good, var))
                            break
                    except KeyError: 
                        pass  # if key error, clearly there is no match... 

                    # Look for a matching variant among goods
                    try:
                        if core.series_allclose(lay.goods[good].dropna(), self.lay.goods[(good, var)].dropna(),
                                                allow_empty=False, nanisnull=True):
                            matched_variants.log[good] = var
                            print("We have a match for '{}' in goods composition: variant {}".format(good, var))
                            break
                    except KeyError:
                        pass   # if key error, clearly there is no match... 

                # No match, create a new variant
                if not matched_variants[good]:
                    new_variants.loc[good] = max(good_variants) + 1
            else:
                # No variant defined. This will be the first.
                new_variants.loc[good] = 1

        # Add the new variants to the list of variants for the process_model
        variants = combs['variant'].combine_first(new_variants + matched_variants).astype(int)
        new_variants = new_variants[new_variants != 0]

        return new_variants, variants


    def _integrate_process(self, gen):

        def to_act_frame(ds, process_id, var):
            ds = ds.dropna()
            df = ds.to_frame()
            df.columns = pd.MultiIndex.from_tuples([(process_id, var)], names=['act', 'var'])
            return df

        def get_variants(subst, subst2, labels):

            # Initialize
            variants = pd.Series(index=labels, dtype=int)

            # For each product category, find the highest variant ID already defined
            # todo: Will want to take into account lay_goods in addition to find the real max id
            max_id = subst.T.reset_index(level=1)['var'].groupby('id').max()

            # Go through all nonnull columns of the new lay (TODO: only subst for now) and either match with
            # pre-existing variant or define new one
            bo = subst2.any(axis=0)
            for i in subst2.loc[:, bo].columns:
                # Compare the new layer to pre-existing layer values; TODO should be replaced by "allclose" test
                # test if any column [.any()] is fully identical to the new layer, along all its values
                if i in subst.columns:
                    test = subst.loc(axis=1)[i].eq(subst2[i], axis=0)
                    matches = list(test.columns[test.all(axis=0)])
                    if len(matches) == 1:
                        variants[i] = matches[0]
                    elif len(matches) == 0:  # create new variant if no matches
                        variants[i] = max_id[i] + 1
                    else:
                        raise ValueError("More than one matching identical column? Not supposed to be.")
                else:
                    variants[i] = 1

            return variants[variants > 0]

        def apply_variants(variants, factors=False):

            if factors:
                laytot = self.lay_f
                lay = gen.lay_f
            else:
                laytot = self.lay
                lay = gen.lay

            # Todo make as a function
            subst_out = lay.subst[variants.index]
            subst_out.columns = pd.MultiIndex.from_arrays([subst_out.columns, variants.values], names=['id', 'var'])
            new_cols = [j for j in subst_out.columns if j not in laytot.subst.columns]
            laytot.subst = pd.concat([laytot.subst, subst_out[new_cols]], axis=1)
            laytot.subst = laytot.subst.reindex(columns=laytot.subst.columns.levels[0], level=0)


        # Shortcuts
        f = gen.f
        u = gen.u.dropna()
        v = gen.v.dropna()

        variants = get_variants(self.lay.subst, gen.lay.subst, labels=gen.u0.index)
        variants_f = get_variants(self.lay_f.subst, gen.lay_f.subst, labels=gen.f0.index)

        apply_variants(variants, factors=False)
        apply_variants(variants_f, factors=True)

        # Make multiindex - columns
        if gen.process_id in self.U.columns.levels[0]:
            var = np.max(self.U.loc(axis=1)[gen.process_id].columns.values) + 1
        else:
            var = 1
        u = to_act_frame(u, gen.process_id, var)
        v = to_act_frame(v, gen.process_id, var)
        f = to_act_frame(f, gen.process_id, var)

        # Make multiindex - rows
        u.index = pd.MultiIndex.from_arrays([u.index, variants[u.index]])
        v.index = pd.MultiIndex.from_arrays([v.index, variants[v.index]])
        f.index = pd.MultiIndex.from_arrays([f.index, variants_f[f.index]])
        self.U = pd.concat([self.U, u], axis=1)
        self.V = pd.concat([self.V, v], axis=1)
        self.F = pd.concat([self.F, f], axis=1)

    def generate_product_mix(self, the_relevant_indexes):
        """From a set of product grades, ensure that mixes:
            * meet minimal criteria of all users
            * All grades can end up, in some dilution, in all products"""

        # Select the "virgin" product, without having to hardcode/identify
        diulter = select_diluter(self.lay, self.const_max, self.const_min)

        # calculate the mix
        for m in all_materials:
            cols = [diluter, m.index]
            mix, lay_mix = calc_mixing_ratios(lay[cols],
                                              const_max[cols],
                                              const_min[cols])

        return requirements, compositions

    def append_to_productsystem(self, requirements, composition):
        """ Integrate new technology in the product system
            * Add columns for new technologies
            * Add new rows for new products
            * Add new product compositions
        """

    def combine_foregound_and_background(self):
        self.A = pd.concat((self.V - self.U, self.A_bb), axis=1, sort=False).fillna(0.0)
        self.F = pd.concat((self.F_f, self.F_b), axis=1, sort=False).fillna(0.0)
        self.y = self.y.reindex(index=self.A.index).fillna(0.0)

    def solve_system(self):
        """ Apply LP to identify production volumes"""
        sol = linprog(self.C @ self.F, A_eq=self.A, b_eq=self.y) #, bounds=self.bounds)

        if sol['success']:
            self.x = pd.Series(sol['x'], index = self.A.columns)
            self.sol = sol
        else:
            print('FAIL')
            print(sol)

    def calc_impact(self):
        return self.C @ self.F @ self.x

    def _find_combinations_of_variants_of_determining_flows(self, gen, ix=None):
        """ Make a cartesian product of all variants of determining flows of process

        Args
        ----
        gen : ProcessModel instance
            Provide the list of determining flows
        ix : [optional] Pandas Dataframe multiindex (levels: goods, variants)
            Typically index of A-matrix.
            If ix is None, it is automatically defined from self.A.index

        Returns
        -------
        combinations : Dataframe of possible combinations (columns) of product variants (rows)

        """
        # Get index
        if ix is None:
            ix = self.A.index

        unmatched_flows = set(gen.determining_goods_flows) - set(ix.get_level_values(0))
        if unmatched_flows:
            msg = "The following determining inputs of {} do not seem to have defined compositions: {}"
            raise ValueError(msg.format(gen.process_id, unmatched_flows))

        # Get the data for determining flows
        ix_det = ix.to_frame().loc(axis=0)[gen.determining_goods_flows, :].reset_index(drop=True)

        # Prepare for cartesian product
        variants = ix_det.groupby('index')['variant'].apply(np.array)

        # Find all combinations
        combinations = _pd_cartesian_product(variants)
        return combinations



def select_diluter(lay, const_max, const_min):
    """ Function to avoid hardcoding which materials aver virgin

    Select subset of materials that are withing the constraints

    If only one material, that one will be used to dilute the others that don't
    respect constraints when undiluted.

    If more than one within constraints, pick the one that maximizes distance
    to constraint

    """

    # Cannot exceed any of the constraints
    bo = np.prod((lay < const_max) * (lay > const_min), 0)

    # filter
    lay = lay[bo]
    const_max = const_max[bo]
    const_min = const_min[bo]

    # Of remaining (if more than one remains):
    if lay.shape[1] == 0:
        raise ValueError("No single material respects all constraints")

    elif lay.shape[1] == 1:
        diluter = lay.index.values

    elif lay.shape[1] > 1:
        # pick entry that with the largest sum of relative difference to constraints
        # can introduce an exponent between zero and 1 to penalize
        # materials with

        diluter = thewinner.index.values

    return diluter


def calc_mixing_ratios(lay, const_max, const_min):
    """

    min [0, 1] [x_diluter, x_contaminated].T

    s.t.
        lay.dot(x) > const_min
        lay.dot(x) < const_max

    return x, lay.dot(x)
    """

    raise NotImplementedError()


def define_from_template(filename):

    exp = _read_template(filename)


    generators = []
    for act in exp['l_act'].index:
        generators += [_generate_process_from_template_dict(act, exp)]

    y = exp['y']

    lay0, lay_f0 = _generate_layers_from_template_dict(exp)

    system = MarcotModel(generators, y, lay0, lay_f0)
    system.finalize_system_init()

    return system

def load_generator(generator, layer, layer_f, init=True):
    lay = copy.deepcopy(layer)
    lay_f = copy.deepcopy(layer_f)

    # Drop second hierarchical level on columns
    try:
        lay.subst = lay.subst.T.reset_index(level='var', drop=True).T
    except KeyError:
        pass

    try:
        lay_f.subst = lay_f.subst.T.reset_index(level='var', drop=True).T
    except KeyError:  # If it already is only single-level
        pass

    # load in generator
    if init:
        generator.lay0 = lay
        generator.lay_f0 = lay_f
    else:
        generator.lay = lay
        generator.lay_f = lay_f

    return generator

def _adapt_gen_to_input_variants(gen, combs, lay):
    """ Pass the compositions (lay) of the product variants of a combination (combs) to generator, and solve

    Args
    ----
    gen : Process Model instance
    combs: Pandas Series
        The variant of each good selected in the combination
    lay: Layer instance


    Returns
    -------
    a : Pandas Series
        A balanced production function

    """
    if lay.isempty():
        pass # nothing to pass to gen
    else:
        # pass lay except entries that contradict combs
        gen.lay0 = lay.select_combs(combs)
    gen.solve()

    # sort out index name & column name, and combine with combinations
    a = gen.vmu.to_frame(gen.process_id).dropna()
    a.index.name = 'index'

    return a
def _generate_process_from_template_dict(act, exp):

    # Initialize generator object
    gen = process_model.ProcessModel(act)

    # vectors
    gen.l_pro = exp['l_pro']
    gen.l_str = exp['l_str']
    gen.l_cons = exp['l_cons']

    # SUT
    u = exp['use'].reindex(index=gen.l_pro)
    gen.u0 = exp['use'][act]
    gen.v0 = exp['supply'][act]
    gen.f0 = exp['ext'][act]

    # Minimal quality control for the only template sheet where indexes are not autofilled
    unexpected_act = set(exp['tc'].index) - set(exp['l_act'].index)
    if len(unexpected_act):
        raise ValueError("The following activies in 'tc' are not found in 'l_act': {}".format(unexpected_act))

    # Arrange transfer coefficient data
    try:
        gen.calc_transfer_coefficients(exp['tc'].loc[act])
    except KeyError:  # no transfer coefficients defined for that activity
        gen.tc_goods = pd.DataFrame()
        gen.tc_subst = pd.DataFrame()
        gen.tc_cons = pd.DataFrame()

    return gen


def _generate_layers_from_template_dict(exp):

    # Initialize layer objects
    lay = layers.Layers(l_subst=exp['l_subst'], l_cons=exp['l_cons'])
    lay_f = layers.Layers(l_subst=exp['l_subst'], l_cons=exp['l_cons'])

    # Populate goods/product layer
    lay.goods = exp['lay_goods']
    lay.subst = exp['lay_subst']
    lay.cons = exp['lay_cons']

    # Populate factors layer
    lay_f.subst = exp['lay_f_subst']
    if 'lay_f_cons' not in exp.keys():
        lay_f.cons = copy.deepcopy(lay.cons)

    return lay, lay_f


def _read_template(filename):

    # Initializations
    exp = dict()
    xls = pd.ExcelFile(filename)

    for sheet in xls.sheet_names:
        # Read and clean up
        data = pd.read_excel(xls, sheet)
        data = data.dropna(0, 'all').dropna(1, 'all')

        # Select proper index
        if len(data) != 0:
            if sheet != 'tc':
                data = data.reset_index(drop=True).set_index('id')
            else:
                data = data.set_index('act')

        exp[sheet] = data

    # Reindex matrices to match dimensions of vectors
    exp['use'] = exp['use'].reindex(index=exp['l_pro'].index, columns=exp['l_act'].index)
    exp['supply'] = exp['supply'].reindex(index=exp['l_pro'].index, columns=exp['l_act'].index)
    exp['ext'] = exp['ext'].reindex(index=exp['l_str'].index, columns=exp['l_act'].index)

    return exp


def _cartesian_product(*arrays):
    """ apply a cartesian product ot a list of 1d arrays"""
    la = len(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[...,i] = a
    return arr.reshape(-1, la)

def _pd_cartesian_product(ds):
    """ Accepts a data series of 1d arrays, performs cartesian product of these arrays, and returns a dataframe """
    return pd.DataFrame(index=ds.index, data=_cartesian_product(*ds).T)
