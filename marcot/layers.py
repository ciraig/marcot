import re
import warnings
import numpy as np
import pandas as pd
from pandas.testing import assert_frame_equal
import pdb
import copy

try:
    import core
except:
    from marcot import core

# I consider myself warned against chained assignment, turn off false alerts
# http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
pd.options.mode.chained_assignment = None  # default='warn'


class Layers(object):
    """ A generic class for representing the composition of goods in terms of
    other goods, substances and ultimately conservative properties"""


    def __init__(self, l_goods=None, l_subst=None, l_cons=None):

        # Hardcoded stuff
        self.__nes = ' n.e.s.'  # not elsewhere specified
        self.__mass_name = 'Total mass [kg]'
        self.__mass_marker = '[kg]'
        self.__mandatory_l_cols = {'name', 'reference_property', 'unit'}
        self.__monetary_terms = ['Euros', 'CAD', 'USD', 'Dollar', '$', '€', 'value', 'economic', 'monetary', 'value',
                                 'price', 'prix', 'valeur', 'monétaire']

        # metadata on substances and conservative properties
        if l_goods is not None:
            self.l_goods = l_goods
        else:
            self.l_goods = pd.DataFrame(columns=self.__mandatory_l_cols)

        if l_subst is not None:
            self.l_subst = l_subst
        else:
            self.l_subst = pd.DataFrame(columns=self.__mandatory_l_cols)

        if l_cons is not None:
            self.l_cons = l_cons
        else:
            self.l_cons = pd.DataFrame(columns=self.__mandatory_l_cols)

        # encapsulated, "protected" layer values
        self.__goods = pd.DataFrame()
        self.__subst = pd.DataFrame()
        self.__cons = pd.DataFrame()

        # encapsulated, archived variables to speed up calculations of
        # self.net_goods()
        self.__goods0 = pd.DataFrame()
        self.__net_goods0 = pd.DataFrame()



    # ==========================================================================
    """ Properties """

    # ==========================================================================
    @property
    def goods(self):
        """ Get or set the goods composition layer

        Property
        ----------
        layer_values: DataFrame [goods x goods]
            The composition of goods (columns) in terms of other goods (rows)

        """
        return self.__goods

    @goods.setter
    def goods(self, df):
        self.reset_layer(goods=True)
        self.add_layer_data(goods = df)


    @property
    def subst(self):
        """ Get or set the substance composition layer

        Property
        ----------
        layer_values: DataFrame [substances x goods]
            The composition of goods (columns) in terms of substances (rows)

        """
        return self.__subst

    @subst.setter
    def subst(self, df):
        self.reset_layer(subst=True)
        self.add_layer_data(subst = df)

    @property
    def substp(self):
        return self._demonetize(self.__subst)

    @property
    def cons(self):
        """ Get or set the conservative property composition layer

        Property
        ----------
        layer_values: DataFrame [conservative x substances]
            The composition of substances (columns) in terms of conservative
            properties (rows)

        """
        return self.__cons

    @cons.setter
    def cons(self, df):
        self.reset_layer(cons=True)
        self.add_layer_data(cons = df)

    @property
    def consp(self):
        return self._demonetize(self.__cons, index=True, columns=True)

    # ==========================================================================
    """ Key Methods """

    # ==========================================================================

    def net_goods(self, drop_intermediates=True, abbreviate=False, keep_self_reference=True):
        """ Calculate the net content of goods in other goods

        For example, if a piston is in a motor, and a motor is in a car,
        identify that the net composition of the car includes the the piston.

        Parameters
        ----------
        drop_intermediates: boolean
            Only express the compositions in terms of goods that are themselves
            not defined in terms of other goods, but rather in terms of
            substances. Alternatively, include everything. This parameter trumps `keep_self_reference`

        abbreviate: boolean
            Drop null rows and columns, to focus on actual data. Alternatively,
            by default, keep full dimensions for calculations

        keep_self_reference : boolean
            If true, each good contains itself (e.g., a car contains *a car*, in addition to a motor, and pistons,
            etc.). If this


        Returns
        --------
        net_composition: DataFrame, [goods x goods]
            The net composition of each good, taking into account that goods
            can be embedded into goods that are embedded into goods...

        Notes
        -----

        This method essentially applies this matrix operation

        .. math:: (I - G)^{-1}

        where :math:`I` is the identity matrix, and :math:`G` is the `lay.goods`
        composition layer.

        However, it is not so straight forward, as the function strive to reuse
        the archived results of any previous calculation to speed up operations.

        """

        def reduce(a):
            """Keep only original columns and non-null rows"""
            b = a.loc[:, ix_org]
            return b.loc[b.sum(1) > 0, :]

        def archive_goods():
            """ Make copies of the last goods and net_goods compositions to
            speed up future calculations"""
            self.__goods0 = self.goods.copy()
            self.__net_goods0 = net_goods.copy()

        # First, harmonize dimensions of layers
        self.harmonize_layers()

        # Test for simplest case: no goods layer defined, return empty
        # dataframe as is
        goods_ix = set(self.goods.index)
        if not goods_ix:
            net_goods = self.goods.copy()
            archive_goods()

        else:  # if goods layer is defined..

            # Identify which goods were already covered in the last calculation and which are new
            goods0_ix = set(self.__goods0.index)
            ix_org = goods0_ix & goods_ix
            ix_new = goods_ix - goods0_ix

            # If dimensions have not changed
            if len(ix_org) == self.goods.shape[0]:
                # And if coefficients have not changed:
                if core.pd_allclose(self.__goods0, self.goods):
                    # Fastest, return previously archived net_goods layer
                    net_goods = self.__net_goods0.copy()
                else:
                    # Else, if coefficients of goods layer has been altered,
                    # fully recalculate and rearchive
                    net_goods = _leontief_goods(self.goods)
                    archive_goods()

            # If new goods added, but composition of goods from previous round is unalterred
            elif bool(ix_org) and  core.pd_allclose(reduce(self.__goods0), reduce(self.goods)):
                # Only calculate compositions of new goods, and reuse archived
                # compositions for speed
                comp_new = pd.concat((
                    _leontief_goods(self.goods.loc[ix_new, ix_new]),
                    self.__net_goods0 @ self.goods.loc[ix_org, ix_new].fillna(0.)
                    ), axis=0, sort=False)

                # Combine with others
                net_goods = pd.concat((self.__net_goods0, comp_new), axis=1, sort=False).fillna(0.0)
                archive_goods()

            else:
                # Calculate all from scratch
                net_goods = _leontief_goods(self.goods)
                archive_goods()

        # Archived layers must be independent from working layers
        fail_message="Archived {}0 is *not* independent from {}. Very Bad."
        assert self.__net_goods0 is not net_goods, fail_message.format('self.__net_goods', 'net_goods')
        assert self.__goods0 is not self.goods, fail_message.format('self.__goods', 'self.goods')
        assert self.__goods0 is not self.__goods, fail_message.format('self.__goods', 'self.__goods')

        #--------------postprocessing--------------------------------

        net_goods = net_goods.fillna(0.)

        if not keep_self_reference:
            net_goods = net_goods - core.eye_like(net_goods)

        if drop_intermediates:
            # Remove contents expressed in terms of goods that are themselves expressed in terms of goods
            # Keep only "ultimate" goods composition, i.e., composition expressed in terms of goods that are themselved
            # defined in terms of subst
            bo = self.goods.sum() != 0.0
            net_goods.loc[bo, :] = 0.0

        if abbreviate:
            # Get rid of null rows and columns
            net_goods = core.abbreviate(net_goods)

        return net_goods


    def net_subst(self, abbreviate=False):
        """ Calculate the overall substance composition of goods

        This function takes into accoun the data in `self.goods` and
        `self.subst` to calculate how much of each substance is in each good.

        Parameters
        ----------
        abbreviate: boolean
            If true, drop null rows and columns, to focus on actual data.
            Alternatively, by default, keep full dimensions for calculations

        Returns
        -------
        net: DataFrame, [subst x goods]
            The net substance composition of goods, taking into account that
            substances can be in goods that can be in goods that can... etc.


        """
        # Quickly re-index layers too make dimensions fit.
        self.harmonize_layers()

        # The composition of a good can be defined in terms of other goods,
        # **or** in terms of substances, but not both, otherwise: confusion
        self.check_sanity()


        # Main calculation
        if np.any(self.goods):
            net = core.flexdot(self.subst.fillna(0.), self.net_goods(keep_self_reference=True))
        else:
            net = self.subst.fillna(0.)

        if abbreviate:
            net = core.abbreviate(net)

        return net

    def net_substp(self, abbreviate=False):
        """ Net physical substances """
        return self._demonetize(self.net_subst(abbreviate=abbreviate))


    def net_cons(self, abbreviate=False):
        """ Calculate the overall composition of goods in terms of conservative properties

        Express the content of goods in terms of total mass, chemical elements
        (in absence nuclear reaction), energy, etc. This takes into account
        that elements can be in substances which can be in goods which can be
        in other goods, which can... etc.

        Parameters
        ----------
        abbreviate: boolean
            If true, drop null rows and columns, to focus on actual data.
            Alternatively, by default, keep full dimensions for calculations

        Returns
        -------
        net: DataFrame [cons x goods]
            The net conservative properties (rows) composition of goods (columns)
        """


        # Make layer dimensions fit
        self.harmonize_layers()

        # Main calculation
        net = core.flexdot(self.cons.fillna(0.), self.net_subst())

        if abbreviate:
            net = core.abbreviate(net)

        return net

    def net_consp(self, abbreviate=False):
        """ Like net_con(), but filtering out non-physical (esp. monetary) non-conserved properties.
        Only applies to rows.

        """

        self.harmonize_layers()

        net = core.flexdot(self.consp.fillna(0.), self.net_substp())

        if abbreviate:
            net = core.abbreviate(net)

        return net


    def define_conservative_properties(self, new=None, nan2zero=False):
        """ Define or expand the conservative properties (cons)

        Add new conservative properties to cons and generate associated dummy
        'not elswhere specified' (n.e.s.) substances associated with each
        conservative property

        - Look for cons without associated substances
        - accept new cons
        - for both cases, create associated n.e.s. substances
        - ensure "mass" special case

        For example, if "Carbon" is defined as index of self.cons, then there
        shoul be a column (substance) "Carbon n.e.s."

        Arguments
        ---------
        new: 1-D listlike object
            List of additional conservative properties


        Returns
        ------
        None


        """
        orphans=[]
        for i in self.cons.index:
            # Check if any conservative property is missing its n.e.s-equivalent substance
            if i+self.__nes not in self.cons.columns:
                orphans.append(i)
            # Ensure that every conserved and n.e.s.-equivalent substance is... equivalent, i.e., == 1
            else:
                self.cons.loc[i, i+self.__nes] = 1.0

        #orphans = [i for i in self.cons.index if i+self.__nes not in self.cons.columns]

        # Any new conservative property to be added
        if new is not None:
            new = set(new)
        else:
            # Try to add anything in the labels
            new = set()

        # Warn if any of the "new" conservative properties already exist
        redundant = new & set(self.cons.index)

        # Select only truly new properties from the list
        cons_ix = set(new) - set(self.cons.index)

        # And add orphans
        cons_ix = list(cons_ix | set(orphans))

        # Automatic creation of dummy substances based on new conservative properties
        # add "not elsewhere specified" to substance layer index to avoid confusion
        subst_ix = [i + self.__nes for i in cons_ix]

        # Identity matrix, making link betweeen new conservative properties
        # (rows) and substances (columns)
        I = pd.DataFrame(np.eye(len(cons_ix), dtype='float'), cons_ix, subst_ix)

        # Concat with existing cons
        cons = pd.concat((self.cons, I), axis=1, sort=True)

        # Also,

        # Sort multiple issues with "Total mass [kg]" special property, harmonize dimensions & update list
        self.__cons = self._complement_total_mass_entries(cons)
        self.harmonize_layers()
        subst_ix = [i for i in subst_ix if i in self.l_subst.index]
        cons_ix = [i for i in cons_ix if i != self.__mass_name]

        # For the updated list, complete labels
        self._complement_l_subst_nes()

        if nan2zero:
            self.__cons.fillna(0.0, inplace=True)


    def define_goods_variants(self):
        if len(self.goods):
            self.goods = _initialize_or_expand_variants(self.goods, index=True, columns=True)
        if len(self.subst):
            self.subst = _initialize_or_expand_variants(self.subst, columns=True)


    def add_layer_data(self, goods=None, subst=None, cons=None, how='loose'):
        """ Try to insert data in layer or, if new index/columns are involved, concatenate data to the layer

        TODO: Will fail (1) on partial column overlaps, and (2) on new rows with existing columns

        Parameters
        ----------

        goods: DataFrame [goods x goods], optional
            Data to be added to the goods layer
        subst: DataFrame [subst x goods], optional
            Data to be added to substance layer
        cons: DataFrame [cons x subst], optional
            Data to be added to conservative property layer

        how: string {replace | loose (default) | update_values | update_nan_or_n.e.s. | update_nan}

        Returns
        -------
        None

        NB.: insertion not applicable to cons, only concatenation

        N.B. : way of speeding up: do the update comparison test inside the prepare_insert_data

        """



        # ------preprocessing for data UPDATES--------
        if 'update' in how:
            goods = _prepare_insert_for_data_update(goods, self.goods, how)
            subst = _prepare_insert_for_data_update(subst, self.subst, how)


        # ------update or concatenation--------------
        if goods is not None and len(goods):
            try:
                if how == 'replace':
                    # clear all in relevant columns if in replacing mode
                    self.__goods[goods.columns] = np.nan  
                self.__goods.loc[goods.index, goods.columns] = goods
            except KeyError:
                self.__goods = concat_layer(self.goods, goods)

        if subst is not None and len(subst):
            try:
                if how == 'replace':
                    # clear all in relevant columns if in replacing mode
                    self.__subst[subst.columns] = np.nan
                self.__subst.loc[subst.index, subst.columns] = subst
            except KeyError:
                self.__subst = concat_layer(self.subst, subst)

        # Insrt cons data
        if cons is not None and len(cons):
            self.__cons = concat_layer(self.cons, cons)
            self.define_conservative_properties()


    def reset_layer(self, goods=False, subst=False, cons=False):
        """ Sets layers back to empty dataframes

        Parameters
        ----------
        goods: boolean
            If True, set `self.goods` layer to empty dataframe
        subst: boolean
            If True, set `self.subst` layer to empty dataframe
        cons: boolean
            If True, set `self.cons` layer to empty dataframe

        Returns
        -------
        None

        """

        if goods:
            self.__goods = pd.DataFrame()
        if subst:
            self.__subst = pd.DataFrame()
        if cons:
            self.__cons = pd.DataFrame()



    def check_sanity(self):
        """ Make sure that there is no ambiguous composition data

        Ensure that no goods is defined both in terms of goods _and_ in
        terms of substance or cons.


        Returns
        ------
        None


        Raises
        ------
        ValueError

        """


        goods_composition_defined = set(self.goods.columns[self.goods.sum() > 0.])
        subst_composition_defined = set(self.subst.columns[self.subst.sum() > 0.])
        double_counting = goods_composition_defined & subst_composition_defined

        if double_counting:
            raise ValueError("The composition of the following goods is"
                    " expressed *both* in terms of other goods *and* of"
                    " substances, leading to confusion and risk of double"
                    " counting: {}".format(double_counting))

    def harmonize_layers(self, fill_value=None):
        """ If indexes differ between goods, subst and cons layers, harmonize

        """
        # TODO: take advantage of the definition of l_subst, l_cons, and l_goods now...

        if fill_value is None:
            fill_value = np.nan

        goods_in_subst = set(self.subst.columns)


        # HARMONIZE GOODS

        # If self.subst not defined and rows and cols of self.goods are in
        # agreement, pass
        if not goods_in_subst and (self.goods.index == self.goods.columns).all():
            pass

        # If there is no significant goods layer anyway, leave well alone
        elif not np.any(self.goods):
            self.goods = pd.DataFrame()

        else:
            # Harmonize all indexes and colums defined in terms of goods
            goods1 = set(self.goods.index)
            goods2 = set(self.goods.columns)
            # If differences exist in terms of goods in indexes, reindex
            # `self.goods` and, if defined, `self.subst`
            if (goods1 ^ goods2) | (goods1 ^ goods_in_subst):
                ix = goods1 | goods2 | goods_in_subst
                self.__goods = self.goods.reindex(index=ix, columns=ix, fill_value=fill_value)
                if goods_in_subst:
                    self.__subst = self.subst.reindex(columns=ix, fill_value=fill_value)

        # HARMONIZE SUBSTANCES
        self.reindex_subst(fill_value=fill_value)


    def reindex_subst(self, ix=None, fill_value=0.0, force_drop=False):
        """ Deprecated"""

        # DEPRECATED
        # TODO: switch default fill_value I think...

        # Harmonize substances in indexes and columns
        subst1 = set(self.subst.index)
        subst2 = set(self.cons.columns)

        # If a new index is defined...
        if ix is not None:

            #  Perform a safety check
            if not force_drop:
                ix = set(ix)
                if (subst1 - ix) or (subst2 - ix - {self.__mass_name}):
                    raise ValueError("Reindexing would cause substances to disappear")

        # Otherwise, ix will serve to unify the list of substances, to
        # harmonize the subt and cons matrices
        elif (subst1 ^ subst2) - {self.__mass_name} :
            ix = subst1 | subst2

        # Otherwise, ix empty set, nothing to do
        else:
            ix = set()

        # Apply, if pertinent
        if ix:
            if subst1:
                self.__subst = self.subst.reindex(index=ix, fill_value=fill_value)
            if subst2:
                self.__cons = self.cons.reindex(columns=ix, fill_value=fill_value)

            # TODO: make this more robust
            self.l_subst = self.l_subst.reindex(index=ix, fill_value=fill_value)

    def reindex(self, axis, ix=None, fill_value=None, force_drop=False, force_expand=False):

        # Default values

        if fill_value is None:
            fill_value = np.nan
        corr = set()

        # Get list of axes to reindex and any potential axis-specific correction
        if axis == 'subst':
            # Harmonize substances in indexes and columns
            ax = [set(self.subst.index), set(self.cons.columns), set(self.l_subst.index)]
            corr = {self.__mass_name}
        if axis == 'goods':
            ax = [set(self.goods.index), set(self.goods.columns), set(self.subst.columns), set(self.l_goods.index)]
        if axis == 'factors':
            ax = [set(self.subst.columns)]
        if axis == 'cons':
            ax = [set(self.cons.index), set(self.l_cons.index)]

        union = set.union(*ax)

        # If a new index is defined...
        if ix is not None:

            #  Perform a safety check
            if not force_drop:
                ix = set(ix)
                if union - ix - corr:
                    raise ValueError("Reindexing would cause entities or properties to disappear")

        # Otherwise, ix will serve to unify the list of substances, to
        # harmonize the subt and cons matrices
        elif set.union(*[ax[0] ^ i for i in ax]) - corr:
            ix = union

        # Otherwise, ix empty set, nothing to do
        else:
            ix = set()

        # Apply, if pertinent
        if ix:
            if axis== 'subst':
                if ax[0] or force_expand:
                    self.__subst = self.subst.reindex(index=ix, fill_value=fill_value)
                if ax[1] or force_expand:
                    self.__cons = self.cons.reindex(columns=ix, fill_value=fill_value)
                if ax[2] or force_expand:
                    self.l_subst = self.l_subst.reindex(index=ix, fill_value=fill_value)

            elif axis == 'goods':
                if ax[0] | ax[1] or force_expand:
                    self.__goods = self.goods.reindex(index=ix, columns=ix, fill_value=fill_value)
                if ax[2] or force_expand:
                    self.__subst = self.subst.reindex(columns=ix, fill_value=fill_value)
                if ax[3] or force_expand:
                    self.l_goods = self.l_goods.reindex(index=ix, fill_value=fill_value)

            elif axis == 'factors':
                if ax[0] or force_expand:
                    self.__subst = self.subst.reindex(columns=ix, fill_value=fill_value)

            elif axis == 'cons':
                if ax[0] or force_expand:
                    self.__cons = self.cons.reindex(index=ix, fill_value=fill_value)
                if ax[1] or force_expand:
                    self.l_cons = self.l_cons.reindex(index=ix, fill_value=fill_value)




    def is_waste_or_removal(self):
        self.harmonize_layers()
        return (self.goods < 0).any() | (self.subst < 0).any()

    def select_combs(self, combs):
        """ Select a reduced layer object with only the selected combination of variants


        """


        # Copy whole layer object
        out = copy.deepcopy(self)

        # Select the specified variants of each product
        #   n.b. based solely on subst (not goods) as there must be a composition difference for a variant to exist
        ix = combs.reindex(self.subst.columns.get_level_values(0))

        # By default, select variant one of all other products. 
        # TODO: refine this for goods-variants within selected goods-variants situations
        ix = ix.fillna(1).reset_index().drop_duplicates()

        # Turn into multi-index, and select, then keep only single index
        muix = pd.MultiIndex.from_arrays(ix.values.T, names=ix.columns)
        if not out.subst.empty:
            out.subst = self.subst.loc[:, muix]
            out.subst.columns = out.subst.columns.droplevel('variant')
        if not out.goods.empty:
            out.goods = self.goods.loc[muix, muix]
            out.goods.columns = out.goods.columns.droplevel('variant')
            out.goods.index = out.goods.index.droplevel('variant')

        # Return reduced version of layers
        return out

    # ==========================================================================
    """ Convenience Methods"""

    def get_variants(self, goods_id):

        variants = set()

        try:
            variants |= set(self.goods[goods_id].columns)
        except KeyError:
            pass
        except AttributeError:
            raise ValueError("`get_variants()` is only applicable to layers defined with multiindexed columns")

        try:
            variants |= set(self.subst[goods_id].columns)
        except KeyError:
            pass
        except AttributeError:
            raise ValueError("`get_variants()` is only applicable to layers defined with multiindexed columns")

        return list(variants)



    def isempty(self, detailed=False):

        if not detailed:
            if len(self.goods) + len(self.subst) == 0:
                return True
            else:
                return False
        if detailed:
            self.harmonize_layers()
            return self.subst.isna().all() & self.goods.isna().all()


    def fillna(self, value):
        for l in [self.goods, self.subst, self.cons]:
            l.fillna(value, inplace=True)

    def reindex_like(self, other):
        """ Reindex all matrices of a layer object like that of another layer object"""
        # Update labels
        self.l_goods = other.l_goods.copy(deep=True)
        self.l_subst = other.l_subst.copy(deep=True)
        self.l_cons = other.l_cons.copy(deep=True)

        # Reindex three core layer matrices
        self.subst = self.subst.reindex_like(other.subst)
        self.goods = self.goods.reindex_like(other.goods)
        self.cons = self.cons.reindex_like(other.cons)


    def effective(self, layer, described_entities=None):
        """ Determine whether a property is effectively being used"""

        # By default, look at the description of all goods
        if described_entities is None:
            described_entities = []

        if layer == 'goods' and np.any(self.goods):
            net_comp = self.net_goods(drop_intermediates=False, keep_self_reference=False)
            if described_entities:
                net_comp = net_comp.loc[:, set(net_comp.columns) & set(described_entities)]
            eff = list(set(net_comp.index[net_comp.sum(1) != 0.]) | set(described_entities))

        elif layer == 'subst':
            if described_entities:
                subst=self.subst.loc[:, described_entities]
            else:
                subst = self.subst
            eff = subst.index[subst.fillna(.0).sum(1) != 0]

        elif layer == 'cons':
            eff = self.cons.index[self.cons.fillna(.0).sum(1) != 0 ]

        else:
            raise ValueError("layer must be 'goods', 'subst' or 'cons'")

        return list(eff)

    def drop(self, goods=None, subst=None, goods_cols_also=False, factors=None, useless_goods=False):
        """ Drop a specified or automatically detected list of goods or subst from all layer matrices"""
        if useless_goods:
            goods = set(self.subst.columns[self.subst.isna().all()]) & set(self.goods.columns[self.goods.isna().all()]) & set(self.goods.index[self.goods.T.isna().all()])
            goods_cols_also = True

        if goods:
            self.goods = self.goods.drop(goods, axis='index')
            # If goods are completely useless --- i.e., useless not only as properties (goods within other goods)---
            # remove completely from object... except from label
            if goods_cols_also:
                self.goods = self.goods.drop(goods, axis='columns')
                self.subst = self.subst.drop(goods, axis='columns')

        if factors:
            self.subst = self.subst.drop(factors, axis='columns')
        if subst:
            self.subst.drop(subst, axis='index')
            self.cons.drop(subst, axis='columns')


    # ==========================================================================
    """ Internal Methods """

    # ==========================================================================
    def _demonetize(self, alayer, index=True, columns=False): 
        """ Filter out monetary properperties based on keywords or units, in labels or indexes

        Parameters
        ----------
        alayer: a layer dataframe (e.g., self.subst, self.cons)

        """

        # compile search terms, with special characters escaped, and joined by regex 'or' (|)
        monetary_terms = '|'.join([re.escape(i) for i in self.__monetary_terms])

        # Search through all labels
        ix_monetary = []
        for l in [self.l_goods, self.l_subst, self.l_cons]:
            # and all columns
            iscash = pd.Series()
            for col in self.__mandatory_l_cols:
                try:
                    iscash = iscash | l[col].str.contains(monetary_terms, case=False)
                except KeyError:
                    pass
            ix_monetary += list(l.index[iscash])

        # Also search through layer indexes
        if alayer is not None and len(alayer):
            iscash = alayer.index.str.contains(monetary_terms, case=False)
            ix_monetary += list(alayer.index[iscash])

        # Return a filtered out version
        if index:
            physical_entries = set(alayer.index) - set(ix_monetary)
            allphys = alayer.loc[physical_entries, :]

        if columns:
            physical_entries = set(alayer.columns) - set(ix_monetary)
            allphys = alayer.loc[:, physical_entries]

        # Quality check indexes: Test that each column is _either_ fully positive _or_ negative, but not a mix of the
        # two, which would likely indicate a physical property (e.g., positive waste mass) opposite to a monetary
        # property (e.g., negative value of waste)
        test = allphys.fillna(0.)
        bo = (test >= 0).all() | (test <= 0).all()
        if not bo.all():
            raise ValueError("The following columns have contradictory signs, indicating that some monetary properties"
                             " have likely not been caught by the default filters: {}.".format(bo[~bo].index))

        return allphys


    def _complement_total_mass_entries(self, cons, calc_best_guess=True):
        """ Handle the 'Total Mass' row of the conservative property layer"""


        # First drop any "Total mass" column created accidently
        cons = cons.loc[:, ~ cons.columns.str.contains(self.__mass_name, regex=False)]

        # If composition is expressed on a per-kilogram-of-substance basis, make
        # sure 'Total mass [kg]' row equals one, unless previously defined otherwise.
        is_perkg = cons.columns.str.contains(self.__mass_marker, regex=False)
        mass_row = cons.loc[self.__mass_name]
        is_nullmass = mass_row.isnull() | (mass_row == 0.0)
        cons.loc[self.__mass_name, (is_perkg & is_nullmass)] = 1.0

        if calc_best_guess:
            # If 'Total mass' is null, and yet there are massic compositions
            # (e.g., Cu [kg] content), calculate the total mass from these
            # compositions
            is_nullmass = mass_row.isnull() | mass_row == 0.0
            mass_inputs = [i for i in cons.index if self.__mass_marker in i and i != self.__mass_name] 
            tot_mass_inputs = cons.loc[mass_inputs, :].sum()
            found_mass_inputs = tot_mass_inputs > 0
            bo_need_fix = found_mass_inputs & is_nullmass
            cons.loc[self.__mass_name, bo_need_fix] = tot_mass_inputs.loc[bo_need_fix]
        return cons

    def _complement_l_subst_nes(self):

        bo_nes = self.l_subst.index.str.contains(self.__nes)

        # Easiest insertion
        self.l_subst.loc[bo_nes, 'Distinction'] = self.__nes

        for col in self.__mandatory_l_cols:
            # Create filters
            bo_nan = self.l_subst[col].isna()
            ix_nes = self.l_subst.index[bo_nes & bo_nan]
            ix_plain = [i[:-1*(len(self.__nes))] for i in ix_nes]

            # Fill values
            if col == 'reference_property':
                self.l_subst.loc[ix_nes, 'reference_property'] = self.l_cons.loc[self.__mass_name, 'name']
            else:
                self.l_subst.loc[ix_nes, col] = self.l_cons.loc[ix_plain, col].values

    
    #Are they necessary or something like this already exist ?
    def _add_cons_tc(self,df):
        """
        A function that add goods and substances to lay when goods are defined in term of the conservative layer.
        
        Args
        ----
        :df:dataframe with goods in columns and conservatives in index
        
        Returns
        ----
        :
        
        """
        self.add_layer_data(goods = pd.DataFrame(index = df.columns,columns =df.columns).fillna(value=0.))
        df.index = df.index + (' n.e.s.')
        self.add_layer_data(subst = df)
        return ;
    
    def _balance_cons(self,df):
        """
        A function that balance the mass of a dataframe. 
        
        Args
        ----
        df: dataframe 
            if a value is in mass, its units needs to be specified in the index as [kg]
        
        Returns
        ----
        balance_df: dataframe
            balances data
        
        """
        cons_mass = df.loc[[i for i in df.index if '[kg]' in i]] 
        balance_mass = cons_mass.divide(cons_mass.sum()).fillna(value=0.)
        cons_not_mass = df.loc[[i for i in df.index if i not in cons_mass.index]]
        balance_df = pd.concat([balance_mass,cons_not_mass])
        
        return(balance_df);

def concat_layer(old, new, nan2zero=False):

    # Get datatype issues out of the way
    new = new.astype(float)

    # Default: do nothing
    out = old

    # Initial tests
    redundant = set(old.columns) & set(new.columns)


    if redundant:
        # If now rows anyway, just concatenate vertically
        if len(old) == 0:
            out = pd.concat((old, new), axis=0, sort=False)
        else:
            # Not a clean concatenation. Refuse if overlapping columns
            print("Warning, these columns already exist: {}".format(redundant))
    # All fine, concat
    else:
        out = pd.concat((old, new), axis=1, sort=False)
        if nan2zero:
            out = out.fillna(0.0)

    return out

def harmonize_substances(layer_objects):
    """ Harmonize list of substances across layer objects

    """

    # Create union of all substance lists
    ix = set()
    for lay in layer_objects:
        ix |= set(lay.subst.index)
        ix |= set(lay.cons.columns)

    # Reindex all based on this unified list
    for i in range(len(layer_objects)):
        layer_objects[i].reindex_subst(ix)

    return layer_objects


def harmonize_conservative_properties(layer_objects):
    """ Harmonize the dimensions of cons matrix across multiple layer objects

    """
    # Create union of all conserved property lists
    ix = set()
    for lay in layer_objects:
        ix |= set(lay.cons.index)

    # Apply to all cons matrices in each layer object
    for i in range(len(layer_objects)):
        layer_objects[i].define_conservative_properties(ix)

    return layer_objects





def _leontief_goods(goods):
    """ Calculate total set of goods embedded in each good
    """
    # Define Identity matrix
    I = core.eye_like(goods)

    # Calculate goods cummulatively integrated _inside_ each good:
    #     comp_good = itself (I) + lay_good + lay_good**2 + lay_good**3 + ...
    # which is equivalent to:
    #     comp_good = (I - lay_good)**-1
    return  core.df_inv(I - goods.fillna(0.))


def _check_insertion_rule(layer0, layer_updated, rule):

    #initialize
    e = ''

    # If we are allowed to change n.e.s. entries, remove these from comparison
    if rule == 'update_nan_or_nes':
        not_nes = [i for i in layer0.index if 'n.e.s' not in i]
        layer0 = layer0.loc[not_nes]
        layer_updated = layer_updated.loc[not_nes]

    if rule == 'update_nan' or rule == 'update_nan_or_nes':
        # identify data that were NaN and are therefore allowed to change
        was_na = layer0.isna()

        # Masking these NaN, nothing else should be different
        try:
            assert_frame_equal(layer0.mask(was_na), layer_updated.mask(was_na))
        except AssertionError as err:
            e = str(err)
        if e:
            if rule == 'updated_nan_or_nes':
                problem = 'non-NaN, non-n.e.s.'
            else:
                problem = 'non-NaN'
            raise ValueError("Actual ({}) layer data would get overwritten. See difference: \n {}".format(problem, e))


def _prepare_insert_for_data_update(xins, xorg, how):
        # TODO: could offer a version with-automatic-trim option. maybe. one day. by popular demand

    if xins is not None and len(xins):

        # Initialize
        e = ''
        msg = "With how={}, I refuse to add dimensions to property layer. New {}: {}"


        # First, quality check for the dimensions, if not empty dataframe
        extracols = set(xins.columns) - set(xorg.columns)
        extrarows = set(xins.index) - set(xorg.index)
        if extracols:
            raise ValueError(msg.format(how, 'flows', extracols))
        if extrarows:
            raise ValueError(msg.format(how, 'properties', extrarows))

        # replace NaNs in the inserted by any value already present in the destination matrix
        # that way, only real values are inserted, NaNs are not inserted and have no effect.
        xorg = xorg.reindex_like(xins)
        xins = xorg.where(xins.isna(), other=xins)

        # Quality check
        was_na = xorg.isna()
        try:
            if how == 'update_nan_or_nes':
                case = 'non-NaN, non-n.e.s.'
                not_nes = [i for i in xorg.index if 'n.e.s.' not in i]
                assert_frame_equal(xins.mask(was_na).loc[not_nes], xorg.mask(was_na).loc[not_nes])
            else:
                case = 'non-NaN'
                assert_frame_equal(xins.mask(was_na), xorg.mask(was_na))
        except AssertionError as err:
            e = str(err)

        if e:
            raise ValueError("Actual ({}) layer data would get overwritten. See difference: \n {}".format(case, e))

    return xins

def _initialize_or_expand_variants(x, variants=None, index=False, columns=False, fill_value=1):
    """
    Initialize or insert values in the 'variants' level of the multiindex of a layer matrix

    Args
    -----
    x : dataframe
        The layer data
    variants : data series
        variants to insert, predefined
    index: boolean, default False
        Applies to index, true or false
    columns: boolean, default False
        Applies to columns, true or false
    fill_value: integer, default 1

    Returns
    -------
    out : dataframe
        x with its variants updated

    """

    def ix2varix(ix):
        """ """
        ix = ix.to_frame()

        # Insert known variants
        if variants is not None:
            ix.loc[:, 'variant'] = variants

        # Assign the default value to all others
        if 'variant' not in ix.columns:
            ix['variant'] = int(fill_value)
        else:
            ix['variant'] = ix['variant'].fillna(fill_value).astype(int)

        # Return as multiindex
        return pd.MultiIndex.from_arrays(ix.values.T, names=['index', 'variant'])

    out = x.copy(deep=True)
    if columns:
        out.columns = ix2varix(out.columns)
    if index:
        out.index = ix2varix(out.index)

    return out 
