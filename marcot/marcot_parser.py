""" MaRCOTParser: Prepare foreground inventory LCA data

Parse the MaRCOT template that contains the information for constructing
the foreground model of a LCA.
Read in, prepare the data and build the matrices to make them readily
available to be used in MaRCOT.
"""

import pandas as pd
import numpy as np
import copy
from pathlib import Path
from uuid import UUID
import pdb

try:
    import process_model as pm
    import system_model
    import layers
    import core
except ModuleNotFoundError:
    from marcot import process_model as pm
    from marcot import system_model
    from marcot import layers
    from marcot import core

# Template TODO:
#  - [gmb] Frontpage

# Éventuellement TODO
#  - split background?
#  - browsable background?


names = {'ix_l_goods':'productId',
         'ix_l_cons':'consId',
         'ix_l_noncons':'id', #TODO: find more meaningful label in template than "id"
         'ix_l_fp':'emissionId',
         'ix_l_act' : 'activityId',
         }


class MaRCOTParser:

    def __init__(self, path_to_template, strict=True):
        """
        Define an MaRCOTParser object that goes through the excel template
        defined by the path_to_template.

        Args:
        -----
        path_to_template: str
            path to the MaRCOT template to be read
        """
        self.strict = strict

        # define metadata dataframes
        self.foreground = pd.DataFrame()
        self.background_biosphere = pd.DataFrame()
        self.background_technosphere = pd.DataFrame()
        self.l_goods = pd.DataFrame()
        self.l_act = pd.DataFrame()
        self.l_fp = pd.DataFrame()
        self.l_cons = pd.DataFrame()
        self.l_noncons = pd.DataFrame()
        self.process_metadata = pd.DataFrame()
        self.fp_metadata = pd.DataFrame()



        # define input dataframes
        self.U = pd.DataFrame()
        self.V = pd.DataFrame()
        self.F = pd.DataFrame()
        self.y = pd.DataFrame()
        self.tc = pd.DataFrame()
        self.pc = pd.DataFrame()
        self.lay_goods = pd.DataFrame()
        self.lay_noncons = pd.DataFrame()
        self.lay_f_noncons = pd.DataFrame()
        self.lay_cons = pd.DataFrame()

        # parameters only used for the parser
        self.xls_dict = {}

        # the path of the xlsx template of marcot, adapted for every machines (windows, macOS, linux)
        self.path_to_template = Path(path_to_template)

    # ------------------------------------------------ CORE -----------------------------------------------------------#

    def read_template(self):

        self.build_metadata()
        self.build_layers()
        self.check_indexes()

    def check_indexes(self):

        labels = {'l_cons': self.l_cons, 'l_goods': self.l_goods, 'l_noncons': self.l_noncons, 'l_fp': self.l_fp,
                  'l_act': self.l_act}
        for k, x in labels.items():
            # Basic check against duplicate IDs
            duplicated = x.index.duplicated()
            if duplicated.any():
                ix_duplicated = x.index[duplicated].unique().values
                raise ValueError("Duplicates found in the indexes of {}: {}".format(k, ix_duplicated))

            # Basic check against leading or trailing whitespaces
            if not x.empty:
                needs_trimming = x.index.str.contains("^\s+|\s+$")
                if needs_trimming.any():
                    bad = x.index[needs_trimming].values
                    raise ValueError("Leading or trailing whitespaces in the following indexes of {}: {}".format(k, bad))

    def export_layers(self):
        # Define layer objects
        lay = layers.Layers(self.l_goods, self.l_noncons, self.l_cons)
        lay_f = layers.Layers(self.l_goods, self.l_noncons, self.l_cons)

        # Populate
        # TODO: why are we not using add_layer_data?
        lay.subst = copy.deepcopy(self.lay_noncons)
        lay.goods = copy.deepcopy(self.lay_goods)

        if len(self.lay_cons):
            lay.cons = copy.deepcopy(self.lay_cons)
            lay.define_conservative_properties(new=lay.l_cons.index)  # handles n.e.s. substances & total mass
            lay_f.cons = copy.deepcopy(lay.cons)  # Should be identical
            lay_f.define_conservative_properties(new=lay_f.l_cons.index)  # handles n.e.s. substances & total mass

        # Clean up
        lay.drop(useless_goods=True)

        # Define factor layer objects
        lay_f.add_layer_data(subst=self.lay_f_noncons, how='loose')

        return lay, lay_f


    def export_process_models(self, models=None, initialize_lays=False):
        """ Exports all the read data as a dict of individual process_model objects"""

        # Defaults to empty dict
        if models is None:
            models = dict()

        # Assemble process models, one for each activity with a supply
        for act in self.l_act.index:

            # Use predefined process models (or child thereof) whenever possible, otherwise, define
            try:
                process = models[act]
            except KeyError:
                process = pm.ProcessModel(act)

            # Unproblematic assignments
            if initialize_lays:
                process.lay0 = copy.deepcopy(lay)
                process.lay_f0 = copy.deepcopy(lay_f)
                process.lay = copy.deepcopy(lay)
                process.lay_f = copy.deepcopy(lay_f)
            process.l_pro = self.l_goods #TODO: would be nice to be able to define these as part of the init statement
            process.l_str = self.l_fp

            # Assign if defined for the relevant activity
            try:
                process.u0 = self.U[act].dropna()
            except KeyError:
                pass

            try:
                process.v0 = self.V[act].dropna()
            except KeyError:
                pass

            try:
                process.f0 = self.F[act].dropna()
            except:
                pass

            # Assign transfer coefficients
            if not self.tc.empty:
                tc = self.tc[self.tc.act == act].drop('act', axis=1)
                _ = process.calc_transfer_coefficients(tc)

            if not self.pc.empty:
                try:
                    process.pc = self.pc.loc[[act]].reset_index(drop=True)
                except KeyError:
                    pass

            models[act] = process

        return models

    def quick_system_export(self):

        def quick_expand_index(x, ix=None):
            if ix:
                x = x.reindex(index=ix)
            else:
                x = x.copy(deep=True)

            if 'var' not in x.index.names:
                x['var'] = 1
                x = x.set_index('var', append=True)
            return x.fillna(0.)

        # Read Y
        y = self.y[['final_demand', 'productId']]
        y = y.set_index('productId')
        y.index.names = ['Product ID']


        ## Quick variant
        sm = system_model.MarcotModel([])
        sm.y = quick_expand_index(y)
        sm.U = quick_expand_index(self.U)
        sm.V = quick_expand_index(self.V)
        sm.F_f = self.F.copy(deep=True)

        return sm


    # ---------------------------------------------- METADATA ---------------------------------------------------------#

    def build_metadata(self):
        self.load_template()
        self.build_foreground()
        self.build_background_technosphere()
        self.build_background_biosphere()
        self.build_process_metadata()
        self.build_fp_metadata()
        self.build_l_goods()
        self.build_l_act()
        self.build_l_fp()
        self.build_l_cons()
        self.build_l_noncons()

        if self.strict:
            self.labels_checks()

    def load_template(self):
        self.xls_dict = pd.read_excel(self.path_to_template, sheet_name=None)

    def build_foreground(self):
        self.foreground = template_sheet_treatment(self.xls_dict['foreground'])
        self.foreground.loc[:, 'Destination activity ID'] = self.foreground.loc[:, 'Destination activity ID'].ffill()

    def build_background_technosphere(self):
        self.background_technosphere = template_sheet_treatment(self.xls_dict['background_technosphere'])

    def build_background_biosphere(self):
        self.background_biosphere = template_sheet_treatment(self.xls_dict['background_biosphere'])

    def build_l_goods(self):
        self.l_goods = template_sheet_treatment(self.xls_dict['l_goods'])
        self.l_goods.reference_property.fillna('', inplace=True)
        self.l_goods = self.l_goods.set_index(names['ix_l_goods'])

    def build_l_act(self):
        self.l_act = template_sheet_treatment(self.xls_dict['l_act'])
        self.l_act = self.l_act.set_index(names['ix_l_act'])

    def build_l_fp(self):
        self.l_fp = template_sheet_treatment(self.xls_dict['l_fp'])
        self.l_fp.reference_property.fillna('', inplace=True)
        self.l_fp = self.l_fp.set_index(names['ix_l_fp'])

    def build_l_cons(self):
        self.l_cons = template_sheet_treatment(self.xls_dict['l_cons'])
        self.l_cons = self.l_cons.set_index(names['ix_l_cons'])

    def build_l_noncons(self):
        self.l_noncons = template_sheet_treatment(self.xls_dict['l_noncons'])
        self.l_noncons.reference_property.fillna('', inplace=True)
        self.l_noncons = self.l_noncons.set_index(names['ix_l_noncons'])

    def build_process_metadata(self):

        if self.foreground.empty:
            self.build_foreground()

        columns = ['index', 'Description activity', 'Description product', 'activity geography', 'Unit']

        # Selection & clean up
        process_flows = self.foreground.loc[:, ['Use (U)', 'Supply (V)']]
        bo = np.all((process_flows.isnull()) | (process_flows == 0), axis=1)

        self.process_metadata = self.foreground.loc[bo, columns]
        self.process_metadata = self.process_metadata.fillna('')

        self.process_metadata.index = self.process_metadata.loc[:, 'index'].tolist()
        self.process_metadata = self.process_metadata.drop(['index'], axis=1)


    def build_fp_metadata(self):

        if self.foreground.empty:
            self.build_foreground()

        columns = ['Emission ID', 'Description emission', 'Unit', 'comp', 'subcomp']

        self.fp_metadata = self.foreground[self.foreground.loc[:, 'Emission (F)'].fillna(0) != 0].loc[:, columns]

        self.fp_metadata.index = self.fp_metadata.loc[:, 'Emission ID'].tolist()

    def labels_checks(self):

        # CHECKING l_goods

        if self.l_goods.empty:
            self.build_l_goods()

        mandatory_data = ['name', 'unit']
        if [i for i in pd.DataFrame(self.l_goods.loc[:, mandatory_data]).isnull().any()] != [False]*len(mandatory_data):
            raise Exception('Names and units must be provided for every goods label!')
        if [i for i in pd.DataFrame(self.l_goods.index).isnull().any()] != [False]:
            raise Exception('Provide an index for every goods label')

        # Deprecated if not relying on uuids anymore.

        # if len([i for i in self.l_goods.index.tolist() if not is_valid_uuid(i)]) != 0:
        #     if len([i for i in self.l_goods.index.tolist() if not is_valid_uuid(i)]) == 1:
        #         raise Exception(str([i for i in self.l_goods.index.tolist() if not is_valid_uuid(i)])+
        #                     ' from l_goods is not a valid uuid. Please produce one using a generator on the Internet,'
        #                     ' or using the Python module uuid.')
        #     elif len([i for i in self.l_goods.index.tolist() if not is_valid_uuid(i)]) > 1:
        #         raise Exception(str([i for i in self.l_goods.index.tolist() if not is_valid_uuid(i)])+
        #                     ' from l_goods are not valid uuids. Please replace them using a generator on the'
        #                     ' Internet, or using the Python module uuid.')

    # ------------------------------------------- INPUT MATRICES ------------------------------------------------------#

    def build_layers(self):

        self.build_U_or_V(U=True)
        self.build_U_or_V(V=True)
        self.build_F()
        self.build_y()
        self.build_tc()
        self.build_pc()
        self.build_lay_goods()
        self.build_lay_noncons()
        self.build_lay_f_noncons()
        self.build_lay_cons()

        self.layers_check()

    def build_U_or_V(self, U=False, V=False, force_float=False):

        def build_X(col_name, force_float):

            def refuse(items):
                """ Simply refuse to aggregate anything in the pivot_table function of build_X"""
                if len(items) == 1:
                    return items
                else:
                    msg = "Duplicates in {} in 'foreground' template sheet! These two values have the same indices: {}."
                    raise TemplateError(msg.format(col_name, list(items)))

            # create a dataframe containing data we need
            columns = [col_name] + ['Destination activity ID', 'index']



            # Select subset of relevant data
            is_defined = ~ self.foreground.loc[:, ['Use (U)', 'Supply (V)']].isnull().all(axis=1)
            df = self.foreground.loc[is_defined, columns]

            # if a source activity is not specified (i.e., competition) replace the nan value by an empty value
            # df.loc[:, 'Source activity ID'] = df.loc[:, 'Source activity ID'].fillna('')

            # make the use matrix
            X = pd.pivot_table(df, values=col_name, index=['index'],
                                columns=['Destination activity ID'], aggfunc=refuse)

            if force_float:
                X = X.apply(pd.to_numeric, errors='coerce').astype(float)

            return X.fillna(0)

        if U:
            self.U = build_X('Use (U)', force_float)
        elif V:
            self.V = build_X('Supply (V)', force_float)

        # TODO: I think this is not necessary anymore
        #self.V = V.drop(self.V.loc[[i for i in self.V.index if i not in self.l_goods.index.tolist() and
        #                                 i not in self.background_technosphere.productId.tolist()]].index, axis=0)

    def build_F(self):

        columns = ['Emission (F)', 'Destination activity ID', 'Emission ID']
        df = self.foreground.loc[:, columns]

        self.F = pd.pivot_table(df, values='Emission (F)', index=['Emission ID'],
                                columns=['Destination activity ID'], aggfunc=sum, fill_value=0)
        # Eliminate flows that are not factor of productions (check in l_fp and background)
        self.F = self.F.drop(self.F.loc[[i for i in self.F.index if i not in self.l_fp.index.tolist() and
                                         i not in self.background_biosphere.emissionId.tolist()]].index, axis=0)
        # If no emissions have been entered for a process, the process won't appear in the columns.
        # We thus reintroduce it here. Beware the order of columns is not preserved.
        for process in set(df['Destination activity ID']):
            if process not in self.F.columns.tolist():
                self.F[process] = 0

    def build_y(self):
        self.y = template_sheet_treatment(self.xls_dict['y'])

    def build_tc(self):
        self.tc = template_sheet_treatment(self.xls_dict['tc'])

        # select only useful data
        self.tc = self.tc[['act', 'cons', 'noncons', 'goods ID', 'out ID', 'value']]

        # Rename columns closer to process_model names # TODO: could pick better names
        self.tc.columns = ['act', 'cons', 'subst', 'goods', 'goods_out', 'value']

        # Replace 'act' entry with activity Id
        names = self.l_act[['name', 'geography']].apply(lambda row: ' '.join(row.values), axis=1)
        act_names2ids = dict(zip(names.values, names.index))
        self.tc['act'].replace(act_names2ids, inplace=True)

    def build_pc(self):
        self.pc = template_sheet_treatment(self.xls_dict['pc'])

        # select only useful data
        self.pc = self.pc[['activityId', 'determined flow ID (economic or elementary)', 'value', 'reference flow ID',
                           'property [default = reference property]', 'functional [default=False]']]
        self.pc.set_index('activityId', drop=True, inplace=True)

        # implement default values
        self.pc['functional [default=False]'].fillna(False, inplace=True)
        bo = self.pc['property [default = reference property]'].isnull()
        ref_id = self.pc.loc[bo, 'reference flow ID']
        ref_prop = self.l_goods.loc[ref_id, 'reference_property']
        self.pc.loc[bo, 'property [default = reference property]'] = ref_prop.values

        # Rename columns closer to process_model names # TODO: could pick better names
        self.pc.columns = ['determined flow ID', 'value', 'reference flow ID', 'property', 'functional']

    def build_lay_goods(self):

        lay_goods_sheet = self.xls_dict['lay_goods']
        lay_goods_sheet.productId = lay_goods_sheet.productId.ffill()
        lay_goods_sheet = template_sheet_treatment(lay_goods_sheet)

        columns = ['value', 'productId', 'ID of products inside product']
        df = lay_goods_sheet.loc[:,columns]

        self.lay_goods = pd.pivot_table(df, values='value', index=['ID of products inside product'],
                                        columns=['productId'], aggfunc=sum, fill_value=0)

    def build_lay_noncons(self):

        self.lay_noncons = self.xls_dict['lay_noncons']
        self.lay_noncons = self.lay_noncons.drop([self.lay_noncons.index[0], self.lay_noncons.index[1]], axis=0)
        self.lay_noncons = self.lay_noncons.set_index('id good')
        self.lay_noncons = template_sheet_treatment(self.lay_noncons, col_set=self.l_goods.index)

    def build_lay_f_noncons(self):

        self.lay_f_noncons= self.xls_dict['lay_f_noncons']
        self.lay_f_noncons = self.lay_f_noncons.drop([self.lay_f_noncons.index[0], self.lay_f_noncons.index[1]], axis=0)
        self.lay_f_noncons.index = self.lay_f_noncons.loc[:, 'id production factor']
        self.lay_f_noncons = self.lay_f_noncons.drop('id production factor', axis=1)
        self.lay_f_noncons = template_sheet_treatment(self.lay_f_noncons)

    def build_lay_cons(self):

        self.lay_cons = self.xls_dict['lay_cons']
        self.lay_cons.index = self.lay_cons.loc[:, 'id']
        self.lay_cons = self.lay_cons.drop('id', axis=1)
        self.lay_cons = template_sheet_treatment(self.lay_cons, remove_empty_rowscols=False)

    def layers_check(self):

        # CHECKING tc
        if self.tc.empty:
            self.build_tc()

        # there can only be one! ("Christopher Lambert")
        not_more_than_one = ['cons', 'subst', 'goods']
        erroneous_combinations = [[False, False, True], [False, True, False], [True, False, False],
                                 [False, False, False]]
        for row in self.tc.index:
            if self.tc.loc[row, not_more_than_one].isnull().tolist() in erroneous_combinations:
                raise Exception('Error! Only a conservative property, non conservative property or goods ID can be '
                                'passed at the same time to define a transfert coefficient.')
            elif self.tc.loc[row, not_more_than_one].isnull().tolist() == [True, True, True]:
                raise Exception('Error! You need to provide either a conservative property, a non conservative property'
                                ' or a goods ID to define a transfert coefficient')

        if True in [i for i in self.tc.value > 1] or True in [i for i in self.tc.value < 0]:
            raise Exception('Error! The value of a transfert coefficient can only be between 0 and 1.')

        if True in self.tc.loc[:,'goods_out'].isnull().tolist():
            raise Exception('Error! An output is needed to define a transfert coefficient.')

# ------------------------------------------ SUPPORTING FUNCTIONS -----------------------------------------------------#

def template_sheet_treatment(dataframe, col_set=None, remove_empty_rowscols=True):
    """ Removes empty rows and potential typos created Unnamed columns in the template """
    if remove_empty_rowscols:
        dataframe = dataframe.dropna(how='all', axis=0)

    # Drop unwanted columns
    drop = [i for i in dataframe.columns if core.isnan(i) or 'Unnamed' in i]
    if col_set is not None:
        drop += [i for i in dataframe.columns if i not in col_set]
    dataframe = dataframe.drop(drop, axis=1)

    # Drop unwanted rows and return
    keep = [i for i in dataframe.index if not core.isnan(i)]
    return dataframe.loc[keep, :]


def is_valid_uuid(uuid_to_test):
    try:
        uuid_obj = UUID(uuid_to_test)
    except ValueError:
        return False

    return str(uuid_obj) == uuid_to_test


class TemplateError(Exception):
    pass
