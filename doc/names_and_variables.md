Names and variables


Entities
--------

### Objects

- goods (goods) [**deprecated**: product, pro]
- factors of production (fp) [**deprecated**: extension, stressor, ext, str, \_f]


### Events / technologies

- activities (act)


Intrinsic properties of objects: Lay
------------------------------------

- Composition in terms of other goods (lay.goods)
- Non-conserved properties (lay.noncons)  [**deprecated**: substance, subst]
- Conserved Constituents (lay.cons)


Labels
------

- l\_goods (serves for both self.goods and self.lay.goods)
- l\_fp
- l\_act
- l\_noncons
- l\_cons
- etc.

Others
------

- Transfer coefficients (tc)


Reflection
----------

Alternatives to lay.noncons:

- [rejected] lay.comp for "composition"
  - disadvantage: potential confusion with "compartment" in LCA terminology.
  - disadvantage: monetary value is not a "composition"

- [ambivalent] lay.ncons, for non-conserved properties
  - disadvantage: harder to distinguish visually from lay.cons
  - advantage: shorter

- [ambivalent] lay.prop
  - disadvantage: kind of vague, and redundant relative to definition of lay
  - advantage: short, not defined negatively


