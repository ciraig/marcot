MAterial Rectangular Choice of Technology (MARCOT) model
========================================================

The goal
--------

This project aims to build a computation framework able to:

* Calculate direct and indirect environmental impacts of a production/consumption system (LCA perspective)
* Track the flows and accumulations of mass, energy, and the different chemical elements (MFA perspective)
* Identify optimal decisions under constraints (LP approach)

More specifically, this project focusses on a refined representation of the partial intersubstitutability between products with differing compositions. We aim to capture the consequences of these differences in compositions on the "downstream" activities using these products, and how this in turn affect the compositions of the outputs of these activities, and so on. The hope is to throw down one of the last hurdles preventing a deep integration of MFA and LCA: the lack of mass and elemental conservation in substitution modelling [1].

We build upon excellent previous modelling efforts, employing strategies from:

* the RCOT model [2,3]
* the WasteIO model [4,5]

Further down the line, we aim for an integration with dynamic stock models as drivers [6,7]


Still in very early development
--------------------------------

This tool will be released under an open-source license as soon as the framework functions and the ideas behind it are published. For now, though, it remains more a dream than a functioning software.


Unit tests
----------

The easiest way to run unit tests is to go in the top directory and use unittest from the terminal

```
python -m unittest
```

References
----------

[1] Majeau-Bettez, G. et al., 2016. When Do Allocations and Constructs Respect Material, Energy, Financial, and Production Balances in LCA and EEIO? _Journal of Industrial Ecology_, 20(1), pp.67–84.

[2] Duchin, F. & Levine, S.H., 2011. Sectors May Use Multiple Technologies Simultaneously: the Rectangular Choice-of-Technology Model With Binding Factor Constraints. _Economic Systems Research_, 23(3), pp.281–302.

[3] Kätelhön, A., Bardow, A. & Suh, S., 2016. Stochastic Technology Choice Model for Consequential Life Cycle Assessment. _Environmental Science & Technology_, 50(23), pp.12575–12583.

[4] Nakamura, S. & Kondo, Y., 2002. Input-Output Analysis of Waste Management. _Journal of Industrial Ecology_, 6(1), pp.39–63.

[5] Nakamura, S. et al., 2008. The Waste Input-Output Approach to Materials Flow Analysis. _Journal of Industrial Ecology_, 11(4), pp.50–63.

[6] Pauliuk, S. & Müller, D.B., 2014. The role of in-use stocks in the social metabolism and in climate change mitigation. _Global Environmental Change_, 24(1), pp.132–142.

[7]Pauliuk, S., 2014. pyDSM - a Python class for dynamic stock modelling. Available at: https://github.com/stefanpauliuk/pyDSM [Accessed October 6, 2014].
