import unittest
import pandas as pd
import pandas.util.testing as pdt
import numpy as np
import re
import tabulate
from io import StringIO
try:
    import process_model as pm
except:
    from marcot import process_model as pm

try:
    import layers
except:
    from marcot import layers

import pdb


class TestPM(unittest.TestCase):
    def setUp(self):

        # TODO: need to rework example, to not have substance and cons layer conflict names

        anid = np.random.randint(0, 100)
        # We define an empty gen
        self.gen = pm.ProcessModel(anid)

        # We will track a certain number of properties
        self.layer_ids = ['Total mass [kg]', 'Pb']

        # and a certain number of objects
        self.product_ids = ['waste', 'ore', 'part1', 'part2']

        # The technology uses some inputs
        self.gen.u0 = pd.Series(index=self.product_ids, data=[10.0, 30, 0, 0])

        # And produces some outputs
        self.gen.v0 = pd.Series(index=self.product_ids, data=[0.0, 0.0, 35.0, 5.0])

        self.gen.l_pro = read_tbl("""
                   | name   | reference_property   | unit
            -------+--------+----------------------+--------
             waste | waste  | Total mass [kg]      | kg
             ore   | ore    | Total mass [kg]      | kg
             part1 | part1  | Total mass [kg]      | kg
             part2 | part2  | value                | Euro
            """, dtype=object)

        # These inputs and outputs have a certain composition
        self.gen.lay0.subst = read_tbl("""
                             |   waste |   ore |    part1 |   part2
            -----------------+---------+-------+----------+---------
             Total mass [kg] |     1   |   1   | 1        |    1
             Pb              |     0.2 |   0.1 | 0.135714 |    0.05
            """, dtype='float64')

        # Define substance layer
        self.gen.lay.subst = read_tbl("""
                             |   waste |   ore |   part1 |   part2
             ----------------+---------+-------+---------+---------
             Total mass [kg] |     1   |   1   |     nan |     nan
             Pb              |     0.3 |   0.1 |     nan |     nan
            """, dtype='float64')


        # Define composition of substance layer
        self.gen.lay.cons = read_tbl("""
                                      |   mass nec [kg] |   Pb subst
            --------------------------+-------------------+------
             Total mass [kg]          |                 1 |    0
             Pb                       |                 0 |    1
            """, dtype='float64')

        self.gen.lay0.cons = self.gen.lay.cons.copy()
        self.gen.set_default_constraints()

    def test_check_imbalance(self):
        self.gen.lay0.subst = read_tbl(""" 
                                 |   waste |   ore |   part1 |   part2
                -----------------+---------+-------+---------+---------
                 Total mass [kg] |     1   |   1   |    1    |    1
                 Pb              |     0.2 |   0.1 |    0.13 |    0.05
                """, dtype='float64')
        self.gen.lay0.subst = pd.DataFrame([[1, 1, 1, 1],
                                      [.2, .1, 0.13, 0.05]],
                                     index=self.layer_ids,
                                     columns=self.product_ids)

        imbalance = self.gen.check_balance(init=True)
        the_imbalance = read_tbl("""
                                 |   inputs |   outputs |   factor input |   imbalance
                -----------------+----------+-----------+----------------+-------------
                 Total mass [kg] |       40 |      40   |             -0 |         0
                 Pb              |        5 |       4.8 |             -0 |         0.2
                """, dtype='float64')

        pdt.assert_frame_equal(the_imbalance, imbalance, check_like=True)

    def test_check_balance(self):
        imbalance = self.gen.check_balance(init=True)
        assert imbalance is None

    def test_lay_min(self):
        the_lay_min = read_tbl(""" 
                             |   waste |   ore |   part1 |   part2
            -----------------+---------+-------+---------+---------
             Total mass [kg] |     1   |   1   |       0 |       0
             Pb              |     0.3 |   0.1 |       0 |       0
            """, dtype='float64')
        pdt.assert_frame_equal(the_lay_min, self.gen.lay_min, check_like=True)

    def test_lay_max(self):
        the_lay_max = pd.DataFrame.from_dict(
            {'ore': {'Pb': 0.10000000000000001, 'Total mass [kg]': 1.0},
             'part1': {'Pb': 1e+100, 'Total mass [kg]': 1e+100},
             'part2': {'Pb': 1e+100, 'Total mass [kg]': 1e+100},
             'waste': {'Pb': 0.29999999999999999, 'Total mass [kg]': 1.0}})
        pdt.assert_frame_equal(the_lay_max, self.gen.lay_max, check_like=True)

    def test_u_min(self):
        good = pd.Series(
            {'ore': 0.0, 'part1': 0.0, 'part2': 0.0, 'waste': 0.0})
        pdt.assert_series_equal(good.sort_index(), self.gen.u_min.sort_index())

    def test_u_max(self):
        good = pd.Series({'ore': 1e+100,
                          'part1': 0.0,
                          'part2': 0.0,
                          'waste': 1e+100})
        pdt.assert_series_equal(good.sort_index(), self.gen.u_max.sort_index())

    def test_v_min(self):
        good = pd.Series(
            {'ore': 0.0, 'part1': 0.0, 'part2': 0.0, 'waste': 0.0})
        assert_series_equivalent(good, self.gen.v_min)

    def test_v_max(self):
        good = pd.Series(
            {'ore': 0.0,
             'part1': 1e+100,
             'part2': 1e+100,
             'waste': 0.0})
        assert_series_equivalent(good, self.gen.v_max)

    def test_rebalance_sanity(self):
        """ Does rebalance() lead to a result that satisfies check_balance()"""
        self.gen.rebalance()
        imbalance = self.gen.check_balance(init=False)
        assert (imbalance is None)

    def test_rebalance(self):
        self.gen.rebalance()

        good_u = pd.Series({'waste': 9.431367736413929,
                            'ore': 31.243639813844016,
                            'part1': 0.0,
                            'part2': 0.0,})


        good_v = pd.Series({'waste': 0.0,
                            'ore': 0.0,
                            'part1': 35.86937272663886,
                            'part2': 4.9247133877997955,})

        # Note how composition of waste and ore are identical to that defined
        # in setUp()
        good_lay = read_tbl(""" 
                             |   waste |   ore |   part1 |     part2
            -----------------+---------+-------+---------+-----------
             Total mass [kg] |     1   |   1   | 1       | 0.97582
                        Pb   |     0.3 |   0.1 | 0.15906 | 0.0504349
            """, dtype='float64')

        assert_series_equivalent(good_u, self.gen.u)
        assert_series_equivalent(good_v, self.gen.v)
        pdt.assert_frame_equal(good_lay, self.gen.lay, check_like=True)

    #New unittest
    def test_create_transfer_coefficients(self):  #TODO not finished + test failed
        #Example with no dependance involved, creation of 3 tc matrices
        df_nod = read_tbl(""" 
            | goods   | goods_out          |   value | subst     | cons
        ----+---------+--------------------+---------+-----------+--------
          0 | metal   | rejects            |       1 | nan       | nan
          0 | nan     | recycled materials |       1 | HDPE [kg] | nan
          0 | nan     | gas                |       1 | nan       | C
        """, dtype={'goods': 'object', 'goods_out': 'object', 'value': 'int64', 'subst': 'object', 'cons': 'object'})
        
        goods_nod,subst_nod,cons_nod = self.gen.calc_transfer_coefficients(df_nod)
        
        true_goods_nod = read_tbl(""" 
               |   rejects
        -------+-----------
         metal |         1
        """, dtype='int64')
        
        true_subst_nod = read_tbl(""" 
                   |   recycled materials
        -----------+----------------------
         HDPE [kg] |                    1
        """, dtype='int64')
        
        true_cons_nod = read_tbl(""" 
            |   gas
        ----+-------
         C  |     1
        """, dtype='int64')
        
        #Example with dependencies, no matrices created yet
        df_d = read_tbl(""" 
            | cons   | goods   | goods_out          |   value | subst
        ----+--------+---------+--------------------+---------+-----------
          0 | carbon | metal   | rejects            |       1 | nan
          0 | nan    | nan     | recycled materials |       1 | HDPE [kg]
          0 | C      | nan     | gas                |       1 | nan
        """, dtype={'cons': 'object', 'goods': 'object', 'goods_out': 'object', 'value': 'int64', 'subst': 'object'})
        
        goods_d,subst_d,cons_d = self.gen.calc_transfer_coefficients(df_d)
        
        pdt.assert_frame_equal(true_goods_nod,goods_nod,check_like=True)
        pdt.assert_frame_equal(true_subst_nod,subst_nod,check_like=True)
        pdt.assert_frame_equal(true_cons_nod,cons_nod,check_like=True)
        
        # Assert that the 3 dataframes are null
        pdt.assert_frame_equal(true_cons_nod,cons_nod,check_like=True)

class TestInputsOutputs(unittest.TestCase):
    def setUp(self):
        anid = np.random.randint(0, 100)
        # We define an empty gen
        self.gen = pm.ProcessModel(anid)

        test_data  = read_tbl("""
                                               | u    | v   | f_factorInput   | f_lcaEmission   |   lay_subst
                -------------------------------+------+-----+-----------------+-----------------+-------------
                 steel [kg]                    | 1000 |     |                 |                 |           1
                 other materials [$]           | 3    |     |                 |                 |          10
                 sellable_scrap [kg]           | -2   |     |                 |                 |           1
                 car parts [kg]                |      | 2   |                 |                 |           1
                 car [unit]                    |      | 1   |                 |                 |        1000
                 treatment for toxic waste [$] | 100  |     |                 |                 |          -8
                 IT services [$]               | 10   |     |                 |                 |           0
                 municipal waste [kg]          | -2   |     |                 |                 |           1
                 waste oil [kg]                |      | -1  |                 |                 |           1
                 treatment for oils n.e.c. [$] |      | 4   |                 |                 |          -4
                 coal [kg]                     |      |     | 10              | -10             |           1
                 o2 [kg]                       |      |     | 20              | -20             |           1
                 co2 [kg]                      |      |     | -12             | 12              |           1
                 Labour [$]                    |      |     | 10              | -10             |           0
                """, dtype={'u': 'object', 'v': 'object', 'f_factorInput': 'object', 'f_lcaEmission': 'object',
                            'lay_subst': 'float64'})

        # Convert table to individual variables
        ix_f = -4
        self.u = test_data['u'].iloc[:ix_f]
        self.v = test_data['v'].iloc[:ix_f]
        self.f_factorinput = test_data['f_factorInput'].iloc[ix_f:]
        self.f_lcaemissions = test_data['f_lcaEmission'].iloc[ix_f:]

        self.lay = layers.Layers()
        self.lay_f = layers.Layers()

        subst = pd.DataFrame(data=test_data['lay_subst'].iloc[:ix_f]).T
        subst.index=['mass [kg]']
        self.lay.subst = subst

        tmp = pd.DataFrame(data=test_data['lay_subst'].iloc[ix_f:]).T
        tmp.index=['mass [kg]']
        self.lay_f.subst = tmp

    def test_calc_input_output_wFactorInputSignConvention(self):
        ins, ins_f, outs, outs_f = self.gen.calc_inputs_and_outputs(self.u, self.v, self.f_factorinput, self.lay, self.lay_f)

        true_ins = read_tbl("""
                                           | 0
             ------------------------------+------
             steel [kg]                    | 1000
             other materials [$]           |    3
             sellable_scrap [kg]           |    0
             car parts [kg]                |    0
             car [unit]                    |    0
             treatment for toxic waste [$] |    0
             IT services [$]               |    0
             municipal waste [kg]          |    0
             waste oil [kg]                |    1
             treatment for oils n.e.c. [$] |   -4
            """, dtype='float64').squeeze()

        true_outs = read_tbl("""
                                           |    0
            -------------------------------+------
             steel [kg]                    |   -0
             other materials [$]           |   -0
             sellable_scrap [kg]           |    2
             car parts [kg]                |    2
             car [unit]                    |    1
             treatment for toxic waste [$] | -100
             IT services [$]               |   -0
             municipal waste [kg]          |    2
             waste oil [kg]                |   -0
             treatment for oils n.e.c. [$] |   -0
            """, dtype='float64').squeeze()

        true_ins_f = read_tbl("""
                        |   f_factorInput
            ------------+-----------------
             coal [kg]  |              10
             o2 [kg]    |              20
             co2 [kg]   |               0
             Labour [$] |               0
            """, dtype='float64').squeeze()

        true_outs_f = read_tbl("""
                        |   f_factorInput
            ------------+-----------------
             coal [kg]  |              -0
             o2 [kg]    |              -0
             co2 [kg]   |              12
             Labour [$] |              -0
            """, dtype='float64').squeeze()


        assert_series_equivalent(ins, true_ins)
        assert_series_equivalent(outs, true_outs)
        assert_series_equivalent(outs_f, true_outs_f)
        assert_series_equivalent(ins_f, true_ins_f)

    def test_calc_input_output_wLCAEmissionsSignConvention(self):
        ins, ins_f, outs, outs_f = self.gen.calc_inputs_and_outputs(self.u, self.v, self.f_lcaemissions, self.lay,
                                                                    self.lay_f, f_convention='emissions_output' )

        true_ins = read_tbl("""
                                           | 0
             ------------------------------+------
             steel [kg]                    | 1000
             other materials [$]           |    3
             sellable_scrap [kg]           |    0
             car parts [kg]                |    0
             car [unit]                    |    0
             treatment for toxic waste [$] |    0
             IT services [$]               |    0
             municipal waste [kg]          |    0
             waste oil [kg]                |    1
             treatment for oils n.e.c. [$] |   -4
            """, dtype='float64').squeeze()

        true_outs = read_tbl("""
                                           |    0
            -------------------------------+------
             steel [kg]                    |   -0
             other materials [$]           |   -0
             sellable_scrap [kg]           |    2
             car parts [kg]                |    2
             car [unit]                    |    1
             treatment for toxic waste [$] | -100
             IT services [$]               |   -0
             municipal waste [kg]          |    2
             waste oil [kg]                |   -0
             treatment for oils n.e.c. [$] |   -0
            """, dtype='float64').squeeze()

        true_ins_f = read_tbl("""
                        |               0
            ------------+-----------------
             coal [kg]  |              10
             o2 [kg]    |              20
             co2 [kg]   |               0
             Labour [$] |               0
            """, dtype='float64').squeeze()

        true_outs_f = read_tbl("""
                        |               0
            ------------+-----------------
             coal [kg]  |              -0
             o2 [kg]    |              -0
             co2 [kg]   |              12
             Labour [$] |              -0
            """, dtype='float64').squeeze()


        assert_series_equivalent(ins, true_ins)
        assert_series_equivalent(outs, true_outs)
        assert_series_equivalent(outs_f, true_outs_f)
        assert_series_equivalent(ins_f, true_ins_f)

""" General functions """

def assert_series_equivalent(x, y):
    pdt.assert_series_equal(x.sort_index(), y.sort_index(), check_names=False)

def read_tbl(tbl, dtype=None):

    # Remove any decorative line
    tbl = re.sub('-[-+]+-\n', '', tbl)

    df =  pd.read_csv(StringIO(tbl), index_col=0, sep='|')

    # remove any leading or trailing spaces from index and columns labels
    try:
        df.index = df.index.str.strip()
    except AttributeError:
        pass

    df.index.name.strip()
    df.columns = df.columns.str.strip()
    
    # Convert to right datatype (float, int, etc.)
    if dtype:
        df = df.astype(dtype)
    
    # If any remaining "object" datatype (i.e., string), also strip
    if 'object' in set(df.dtypes.astype(str)):
        df = df.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    
    return df


def to_table_command(df, name=''):
    
    # Convert dataframe
    tbl = tabulate.tabulate(df, tablefmt='presto', headers=df.columns)

    
    # Extract type information
    dtypes = df.dtypes.astype('str')

    # Convert dtype data to string or dict 
    if len(set(dtypes)) == 1:
        dtypes = "'" + dtypes[0] + "'"
    else:
        dtypes = dtypes.to_dict()

    # put together command
    table_command ='{} = read_tbl(""" \n{}\n""", dtype={})'.format( name, tbl, dtypes)
    
    print(table_command)
    
    return table_command




if __name__ == '__main__':
    unittest.main()
