import unittest
import pandas as pd
import pandas.util.testing as pdt
import numpy as np
from io import StringIO
import warnings
import re
try:
    import layers
except:
    from marcot import layers
import pdb
import tabulate
from io import StringIO



class TestLayers(unittest.TestCase):
    def setUp(self): 
        # Ignore import warnings
        warnings.simplefilter('ignore', category=ImportWarning)

        self.lay = layers.Layers()

        # Define goods layer
        self.goods = read_tbl("""
                             |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg]
            -----------------+-----------------+-----------+-------------+-------------+-------------------
             scrap_car [p]   |               0 |       0   |           0 |           0 |                 0
             car [p]         |               1 |       0   |           0 |           0 |                 0
             frame [p]       |               0 |       1   |           0 |           0 |                 0
             motor [p]       |               0 |       1   |           0 |           0 |                 0
             car_wiring [kg] |               0 |       0.5 |           0 |           0 |                 0""",
             'float64')

        # Define substance layer
        self.subst = read_tbl("""
                                   |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg]
            steal_low-alloyed [kg] |               0 |         0 |         700 |          80 |               0
            cast_aluminium [kg]    |               0 |         0 |           0 |          20 |               0
            copper [kg]            |               0 |         0 |           0 |           0 |               0.8
            pvc [kg]               |               0 |         0 |           0 |           0 |               0.  """,
            'float64')

        # Define conservative layer
        self.cons = read_tbl("""
                            |   steal_low-alloyed [kg] |   cast_aluminium [kg] |   copper [kg] |   pvc [kg]
            C [kg]          |                     0.04 |                  0    |             0 |      0.384
            H [kg]          |                     0    |                  0    |             0 |      0.048
            Cl [kg]         |                     0    |                  0    |             0 |      0.568
            Al [kg]         |                     0    |                  0.99 |             0 |      0
            Fe [kg]         |                     0.95 |                  0    |             0 |      0
            Cu [kg]         |                     0    |                  0.01 |             1 |      0
            Total mass [kg] |                     0    |                  0    |             1 |      1 """, 'float64')


    def test_goods_definition(self):
        """ Test that goods property is properly initialized"""
        self.lay.goods = self.goods
        pdt.assert_frame_equal(self.lay.goods, self.goods, check_like=True)

    def test_goods_redefinition(self):
        """ Test that goods property resets cleanly """
        self.lay.goods = self.goods
        self.lay.goods = self.goods
        pdt.assert_frame_equal(self.lay.goods, self.goods, check_like=True)


    def test_subst_definition(self):
        """ Test that subst property is properly initialized"""
        self.lay.subst = self.subst
        pdt.assert_frame_equal(self.lay.subst, self.subst, check_like=True)


    def test_subst_redefinition(self):
        """ Test that subst property resets cleanly"""
        self.lay.subst = self.subst
        self.lay.subst = self.subst  # <-- Note double definition
        pdt.assert_frame_equal(self.lay.subst, self.subst, check_like=True)


    def test_cons_definition(self):
        """ Test initialization of self.cons """
        self.lay.cons = self.cons
        complemented_cons = read_tbl("""
                                   |   Al [kg] |   C [kg] |   Cl [kg] |   Cu [kg] |   Fe [kg] |   H [kg] |   Total mass [kg]
            steal_low-alloyed [kg] |      0    |    0.04  |     0     |      0    |      0.95 |    0     |                 1
            cast_aluminium [kg]    |      0.99 |    0     |     0     |      0.01 |      0    |    0     |                 1
            copper [kg]            |      0    |    0     |     0     |      1    |      0    |    0     |                 1
            pvc [kg]               |      0    |    0.384 |     0.568 |      0    |      0    |    0.048 |                 1
            Cu [kg] n.e.s.         |      0    |    0     |     0     |      1    |      0    |    0     |                 1
            Fe [kg] n.e.s.         |      0    |    0     |     0     |      0    |      1    |    0     |                 1
            H [kg] n.e.s.          |      0    |    0     |     0     |      0    |      0    |    1     |                 1
            Cl [kg] n.e.s.         |      0    |    0     |     1     |      0    |      0    |    0     |                 1
            C [kg] n.e.s.          |      0    |    1     |     0     |      0    |      0    |    0     |                 1
            Al [kg] n.e.s.         |      1    |    0     |     0     |      0    |      0    |    0     |                 1"""
        ).T  # <-- Note the transposition here

        pdt.assert_frame_equal(self.lay.cons, complemented_cons, check_like=True)

    def test_net_goods_default(self):
        self.lay.goods = self.goods

        true_net_goods = read_tbl("""
                             |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg]
            -----------------+-----------------+-----------+-------------+-------------+-------------------
             scrap_car [p]   |             0   |       0   |           0 |           0 |                 0
             car [p]         |             0   |       0   |           0 |           0 |                 0
             frame [p]       |             1   |       1   |           1 |           0 |                 0
             motor [p]       |             1   |       1   |           0 |           1 |                 0
             car_wiring [kg] |             0.5 |       0.5 |           0 |           0 |                 1""", 'float64')
        pdt.assert_frame_equal(self.lay.net_goods(), true_net_goods, check_like=True)

    def test_net_goods_wo_self_reference(self):
        self.lay.goods = self.goods

        true_net_goods = read_tbl("""
                             |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg]
            -----------------+-----------------+-----------+-------------+-------------+-------------------
             scrap_car [p]   |             0   |       0   |           0 |           0 |                 0
             car [p]         |             0   |       0   |           0 |           0 |                 0
             frame [p]       |             1   |       1   |           0 |           0 |                 0
             motor [p]       |             1   |       1   |           0 |           0 |                 0
             car_wiring [kg] |             0.5 |       0.5 |           0 |           0 |                 0""", 'float64')
        pdt.assert_frame_equal(self.lay.net_goods(keep_self_reference=False), true_net_goods, check_like=True)
    def test_harmonize_conservative_properties(self):
        # Define lay
        self.lay.subst = self.subst
        self.lay.cons = self.cons

        # Define lay2
        lay2 = layers.Layers()

        # Define conservative layer
        # Note: C gone, Energy added
        lay2.cons = read_tbl("""
                            |   steal_low-alloyed [kg] |   cast_aluminium [kg] |   copper [kg] |   pvc [kg]
            Energy [MJ]     |                     12   |                  0.4  |             0 |      0.384
            H [kg]          |                     0    |                  0    |             0 |      0.048
            Cl [kg]         |                     0    |                  0    |             0 |      0.568
            Al [kg]         |                     0    |                  0.99 |             0 |      0
            Fe [kg]         |                     0.95 |                  0    |             0 |      0
            Cu [kg]         |                     0    |                  0.01 |             1 |      0
            Total mass [kg] |                     0    |                  0    |             1 |      1 """, 'float64')

        self.lay, lay2 = layers.harmonized_conservative_properties([self.lay, lay2])

        true_lay_cons = read_tbl("""
                                |   Al [kg] |   C [kg] |   Cl [kg] |   Cu [kg] |   Energy [MJ] |   Fe [kg] |   H [kg] |   Total mass [kg]
        ------------------------+-----------+----------+-----------+-----------+---------------+-----------+----------+-------------------
         steal_low-alloyed [kg] |      0    |    0.04  |     0     |      0    |             0 |      0.95 |    0     |                 1
         cast_aluminium [kg]    |      0.99 |    0     |     0     |      0.01 |             0 |      0    |    0     |                 1
         copper [kg]            |      0    |    0     |     0     |      1    |             0 |      0    |    0     |                 1
         pvc [kg]               |      0    |    0.384 |     0.568 |      0    |             0 |      0    |    0.048 |                 1
         Al [kg] n.e.s.         |      1    |    0     |     0     |      0    |             0 |      0    |    0     |                 1
         Cu [kg] n.e.s.         |      0    |    0     |     0     |      1    |             0 |      0    |    0     |                 1
         Fe [kg] n.e.s.         |      0    |    0     |     0     |      0    |             0 |      1    |    0     |                 1
         C [kg] n.e.s.          |      0    |    1     |     0     |      0    |             0 |      0    |    0     |                 1
         H [kg] n.e.s.          |      0    |    0     |     0     |      0    |             0 |      0    |    1     |                 1
         Cl [kg] n.e.s.         |      0    |    0     |     1     |      0    |             0 |      0    |    0     |                 1
         Energy [MJ] n.e.s.     |      0    |    0     |     0     |      0    |             1 |      0    |    0     |                 0
        """, dtype='float64').T


        true_lay2_cons = read_tbl("""
                                |   Al [kg] |   C [kg] |   Cl [kg] |   Cu [kg] |   Energy [MJ] |   Fe [kg] |   H [kg] |   Total mass [kg]
        ------------------------+-----------+----------+-----------+-----------+---------------+-----------+----------+-------------------
         steal_low-alloyed [kg] |      0    |        0 |     0     |      0    |        12     |      0.95 |    0     |                 1
         cast_aluminium [kg]    |      0.99 |        0 |     0     |      0.01 |         0.4   |      0    |    0     |                 1
         copper [kg]            |      0    |        0 |     0     |      1    |         0     |      0    |    0     |                 1
         pvc [kg]               |      0    |        0 |     0.568 |      0    |         0.384 |      0    |    0.048 |                 1
         Al [kg] n.e.s.         |      1    |        0 |     0     |      0    |         0     |      0    |    0     |                 1
         Fe [kg] n.e.s.         |      0    |        0 |     0     |      0    |         0     |      1    |    0     |                 1
         Cu [kg] n.e.s.         |      0    |        0 |     0     |      1    |         0     |      0    |    0     |                 1
         H [kg] n.e.s.          |      0    |        0 |     0     |      0    |         0     |      0    |    1     |                 1
         Energy [MJ] n.e.s.     |      0    |        0 |     0     |      0    |         1     |      0    |    0     |                 0
         Cl [kg] n.e.s.         |      0    |        0 |     1     |      0    |         0     |      0    |    0     |                 1
         C [kg] n.e.s.          |      0    |        1 |     0     |      0    |         0     |      0    |    0     |                 1
        """, dtype='float64').T

        pdt.assert_frame_equal(self.lay.cons, true_lay_cons, check_like=True)
        pdt.assert_frame_equal(lay2.cons, true_lay2_cons, check_like=True)


    def test_harmonize_substances(self):

        # Define lay
        self.lay.subst = self.subst
        self.lay.cons = self.cons

        # Define lay2
        lay2 = layers.Layers()
        lay2.subst = read_tbl("""
                                   |   scrap_car [p] |   motor [p] |   car_wiring [kg]
            stainless steel [kg]   |               0 |          80 |               0
            cast_aluminium [kg]    |               0 |          20 |               0
            copper [kg]            |               0 |           0 |               0.8
            pvc [kg]               |               0 |           0 |               0.2""", 'float64')

        # Harmonize
        lay, lay2 = layers.harmonize_substances([self.lay, lay2])

        # Test result of harmonization


        true_lay2_subst = read_tbl("""
                                    |   scrap_car [p] |   motor [p] |   car_wiring [kg]
            ------------------------+-----------------+-------------+-------------------
             stainless steel [kg]   |               0 |          80 |               0
             steal_low-alloyed [kg] |               0 |           0 |               0
             cast_aluminium [kg]    |               0 |          20 |               0
             copper [kg]            |               0 |           0 |               0.8
             pvc [kg]               |               0 |           0 |               0.2
             Fe [kg] n.e.s.         |               0 |           0 |               0
             H [kg] n.e.s.          |               0 |           0 |               0
             Cl [kg] n.e.s.         |               0 |           0 |               0
             Cu [kg] n.e.s.         |               0 |           0 |               0
             Al [kg] n.e.s.         |               0 |           0 |               0
             C [kg] n.e.s.          |               0 |           0 |               0
            """, dtype='float64')


        true_lay_subst = read_tbl("""
                                    |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg]
            ------------------------+-----------------+-----------+-------------+-------------+-------------------
             stainless steel [kg]   |               0 |         0 |           0 |           0 |               0
             steal_low-alloyed [kg] |               0 |         0 |         700 |          80 |               0
             cast_aluminium [kg]    |               0 |         0 |           0 |          20 |               0
             copper [kg]            |               0 |         0 |           0 |           0 |               0.8
             pvc [kg]               |               0 |         0 |           0 |           0 |               0
             Fe [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0
             H [kg] n.e.s.          |               0 |         0 |           0 |           0 |               0
             Cl [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0
             Cu [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0
             Al [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0
             C [kg] n.e.s.          |               0 |         0 |           0 |           0 |               0
            """, dtype='float64')


        true_lay_cons = read_tbl("""
                                    |   Al [kg] |   C [kg] |   Cl [kg] |   Cu [kg] |   Fe [kg] |   H [kg] |   Total mass [kg]
            ------------------------+-----------+----------+-----------+-----------+-----------+----------+-------------------
             cast_aluminium [kg]    |      0.99 |    0     |     0     |      0.01 |      0    |    0     |                 1
             pvc [kg]               |      0    |    0.384 |     0.568 |      0    |      0    |    0.048 |                 1
             Fe [kg] n.e.s.         |      0    |    0     |     0     |      0    |      1    |    0     |                 1
             H [kg] n.e.s.          |      0    |    0     |     0     |      0    |      0    |    1     |                 1
             stainless steel [kg]   |      0    |    0     |     0     |      0    |      0    |    0     |                 0
             steal_low-alloyed [kg] |      0    |    0.04  |     0     |      0    |      0.95 |    0     |                 1
             Cl [kg] n.e.s.         |      0    |    0     |     1     |      0    |      0    |    0     |                 1
             Cu [kg] n.e.s.         |      0    |    0     |     0     |      1    |      0    |    0     |                 1
             Al [kg] n.e.s.         |      1    |    0     |     0     |      0    |      0    |    0     |                 1
             copper [kg]            |      0    |    0     |     0     |      1    |      0    |    0     |                 1
             C [kg] n.e.s.          |      0    |    1     |     0     |      0    |      0    |    0     |                 1
            """, dtype='float64').T


        pdt.assert_frame_equal(self.lay.subst, true_lay_subst, check_like=True)
        pdt.assert_frame_equal(self.lay.cons, true_lay_cons, check_like=True)
        pdt.assert_frame_equal(lay2.subst, true_lay2_subst, check_like=True)
        
    #New unittests
    def test_add_cons_tc(self):
        # Define lay
        self.lay.goods = self.goods
        self.lay.subst = self.subst
        self.lay.cons = self.cons
        
        #Define new good based on a conservative layer
        new_prod = read_tbl(""" 
                 |   new_product [kg]
        ---------+--------------------
         C [kg]  |                0.1
         H [kg]  |                0.1
         Cl [kg] |                0.1
         Al [kg] |                0.2
         Fe [kg] |                0.2
         Cu [kg] |                0
        """, dtype='float64')
        
        #Use the function
        self.lay._add_cons_tc(new_prod)
        
        true_lay_goods  = read_tbl(""" 
                          |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg] |   new_product [kg]
        ------------------+-----------------+-----------+-------------+-------------+-------------------+--------------------
         scrap_car [p]    |               0 |       0   |           0 |           0 |                 0 |                  0
         car [p]          |               1 |       0   |           0 |           0 |                 0 |                  0
         frame [p]        |               0 |       1   |           0 |           0 |                 0 |                  0
         motor [p]        |               0 |       1   |           0 |           0 |                 0 |                  0
         car_wiring [kg]  |               0 |       0.5 |           0 |           0 |                 0 |                  0
         new_product [kg] |               0 |       0   |           0 |           0 |                 0 |                  0
        """, dtype='float64')
        
        true_lay_subst = read_tbl(""" 
                                |   scrap_car [p] |   car [p] |   frame [p] |   motor [p] |   car_wiring [kg] |   new_product [kg]
        ------------------------+-----------------+-----------+-------------+-------------+-------------------+--------------------
         steal_low-alloyed [kg] |               0 |         0 |         700 |          80 |               0   |                0
         cast_aluminium [kg]    |               0 |         0 |           0 |          20 |               0   |                0
         copper [kg]            |               0 |         0 |           0 |           0 |               0.8 |                0
         pvc [kg]               |               0 |         0 |           0 |           0 |               0   |                0
         C [kg] n.e.s.          |               0 |         0 |           0 |           0 |               0   |                0.1
         H [kg] n.e.s.          |               0 |         0 |           0 |           0 |               0   |                0.1
         Cl [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0   |                0.1
         Al [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0   |                0.2
         Fe [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0   |                0.2
         Cu [kg] n.e.s.         |               0 |         0 |           0 |           0 |               0   |                0
        """, dtype='float64')
        
        true_lay_cons = read_tbl(""" 
                         |   steal_low-alloyed [kg] |   cast_aluminium [kg] |   copper [kg] |   pvc [kg] |   H [kg] n.e.s. |   Cu [kg] n.e.s. |   Al [kg] n.e.s. |   Cl [kg] n.e.s. |   Fe [kg] n.e.s. |   C [kg] n.e.s.
        -----------------+--------------------------+-----------------------+---------------+------------+-----------------+------------------+------------------+------------------+------------------+-----------------
         Al [kg]         |                     0    |                  0.99 |             0 |      0     |               0 |                0 |                1 |                0 |                0 |               0
         C [kg]          |                     0.04 |                  0    |             0 |      0.384 |               0 |                0 |                0 |                0 |                0 |               1
         Cl [kg]         |                     0    |                  0    |             0 |      0.568 |               0 |                0 |                0 |                1 |                0 |               0
         Cu [kg]         |                     0    |                  0.01 |             1 |      0     |               0 |                1 |                0 |                0 |                0 |               0
         Fe [kg]         |                     0.95 |                  0    |             0 |      0     |               0 |                0 |                0 |                0 |                1 |               0
         H [kg]          |                     0    |                  0    |             0 |      0.048 |               1 |                0 |                0 |                0 |                0 |               0
         Total mass [kg] |                     1    |                  1    |             1 |      1     |               1 |                1 |                1 |                1 |                1 |               1
        """, dtype='float64')

        return self.lay.goods,self.lay.subst,self.lay.cons;
        
        pdt.assert_frame_equal(self.lay.goods, true_lay_goods, check_like=True)        
        pdt.assert_frame_equal(self.lay.subst, true_lay_subst, check_like=True)
        pdt.assert_frame_equal(self.lay.cons, true_lay_cons, check_like=True)
        
        
    def test_balance_cons(self): 
        
        df = read_tbl(""" 
                           |   new_product_1 [kg]
        -------------------+----------------------
         C [kg]            |                    8
         H [kg]            |                    7
         Cl [kg]           |                    1
         Al [kg]           |                   10
         Fe [kg]           |                    2
         Cu [kg]           |                    3
         electricite [KWh] |                    4
        """, dtype='int64')
        
        balance_df = read_tbl(""" 
                           |   new_product_1 [kg]
        -------------------+----------------------
         C [kg]            |            0.258065
         H [kg]            |            0.225806
         Cl [kg]           |            0.0322581
         Al [kg]           |            0.322581
         Fe [kg]           |            0.0645161
         Cu [kg]           |            0.0967742
         electricite [KWh] |            4
        """, dtype='float64')
        
        true_balance_df = self.lay._balance_cons(df)
        
        pdt.assert_frame_equal(balance_df,true_balance_df,check_like=True)

    def test_substp(self):

        # Define labels/dimensions
        l_subst = read_tbl("""
                         | name   | reference_property   | unit
        -----------------+--------+----------------------+---------
         subst1 [kg]     | subst1 | mass                 | kg
         subst2 [kg]     | subst2 | mass                 | kg
         foo1 [USD]      | foo1   | bar                  | USD
         foo2 [$]        | foo2   | bar                  | $
         foo4 [Euros]    | foo4   | bar                  | Euros
         foo5 [CAD]      | foo5   | bar                  | CAD
         foo6 [Dollar]   | foo6   | bar                  | Dollar
         foo7 [pistole]  | foo7   | economic value       | pistole
         foo8 [pistole]  | foo8   | price                | pistole
         foo9 [pistole]  | foo9   | value                | pistole
         foo10 [pistole] | foo10  | valeur               | pistole
         foo11 [pistole] | foo11  | prix                 | pistole""", dtype='object')
        l_cons = read_tbl(""" 
                                | name        | unit
        ------------------------+-------------+----------
         Total mass [kg]        | Total mass  | kg
         Carbon [kg]            | Carbon      | kg
         Total value [currency] | Total value | currency""", dtype='object')
        self.lay = layers.Layers(l_subst=l_subst, l_cons=l_cons)

        # Bogus lay.subst for two goods, and accompanying bogus lay.cons
        self.lay.subst = pd.DataFrame(index=l_subst.index,
                                      columns = ['good1', 'good2']).fillna(1.0)
        self.lay.cons = pd.DataFrame(index=l_cons.index,
                                     columns=l_subst.index).fillna(1.0)

        # Expected outcome, with all monetary entries filtered out, based on list of known units (USD, CAD, etc.) or
        # reference properties ('value', 'price', etc.)
        substp = read_tbl("""
                     |   good1 |   good2
        -------------+---------+---------
         subst2 [kg] |       1 |       1
         subst1 [kg] |       1 |       1
        """, dtype='float64')

        # The test
        lay_substp = self.lay.substp
        pdt.assert_frame_equal(lay_substp, substp, check_like=True)

    def test_substp_raising_sign_error(self):

        # Define labels/dimensions (NOTE how we are *not* declaring 'pistole' as a monetary reference_property)
        l_subst = read_tbl("""
                         | name   | reference_property   | unit
        -----------------+--------+----------------------+---------
         subst1 [kg]     | subst1 | mass                 | kg
         subst2 [kg]     | subst2 | mass                 | kg
         foo1 [USD]      | foo1   | bar                  | USD
         foo2 [$]        | foo2   | bar                  | $
         foo4 [Euros]    | foo4   | bar                  | Euros
         foo5 [CAD]      | foo5   | bar                  | CAD
         foo6 [Dollar]   | foo6   | bar                  | Dollar
         foo7 [pistole]  | foo7   |                      | pistole
         foo8 [pistole]  | foo8   |                      | pistole
         foo9 [pistole]  | foo9   |                      | pistole
         foo10 [pistole] | foo10  |                      | pistole
         foo11 [pistole] | foo11  |                      | pistole""", dtype='object')

        l_cons = read_tbl(""" 
                                | name        | unit
        ------------------------+-------------+----------
         Total mass [kg]        | Total mass  | kg
         Carbon [kg]            | Carbon      | kg
         Total value [currency] | Total value | currency""", dtype='object')
        self.lay = layers.Layers(l_subst=l_subst, l_cons=l_cons)

        # Bogus lay.subst for two goods, and accompanying bogus lay.cons
        self.lay.subst = pd.DataFrame(index=l_subst.index,
                                      columns = ['good1', 'good2']).fillna(1.0)

        # NOTE: We give a negative value to one property of goods2
        self.lay.subst.loc['foo7 [pistole]', 'good2'] =  -2
        self.lay.cons = pd.DataFrame(index=l_cons.index,
                                     columns=l_subst.index).fillna(1.0)

        # Expected outcome: substp() will not be able to filter out the "pistole" entries, but will raise error upon
        # realizing that signs contradict for the "physical" properties of goods2
        with self.assertRaises(ValueError):
            foo = self.lay.substp


    def test_consp(self):
        """ Get rid of monetary entries in lay.cons with lay.consp"""

        l_cons = read_tbl(""" 
                                | name        | unit
        ------------------------+-------------+----------
         Total mass [kg]        | Total mass  | kg
         Carbon [kg]            | Carbon      | kg
         Total value [currency] | Total value | currency
        """, dtype='object')

        lay = layers.Layers(l_cons=l_cons)
        lay.cons = pd.DataFrame(index=l_cons.index,
                                columns=['subst1', 'subst2']).fillna(1.0)

        consp = read_tbl(""" 
                         |   subst1 |   subst2 |   Total value [currency] n.e.s. |   Carbon [kg] n.e.s.
        -----------------+----------+----------+---------------------------------+----------------------
         Carbon [kg]     |        1 |        1 |                               0 |                    1
         Total mass [kg] |        1 |        1 |                               0 |                    1
        """, dtype='float64')

    def test_consp_without_lcons(self):
        """ Test that consp also works based strictly on indexes of lay.cons, without lay.l_cons"""

        lay = layers.Layers()

        lay.cons = pd.DataFrame(index=['Total mass [kg]', 'Carbon [kg]', 'Total value [currency]'],
                                columns=['subst1', 'subst2']).fillna(1.0)

        consp = read_tbl(""" 
                         |   subst1 |   subst2 |   Total value [currency] n.e.s. |   Carbon [kg] n.e.s.
        -----------------+----------+----------+---------------------------------+----------------------
         Carbon [kg]     |        1 |        1 |                               0 |                    1
         Total mass [kg] |        1 |        1 |                               0 |                    1
        """, dtype='float64')

        pdt.assert_frame_equal(consp, lay.consp, check_like=True)


def test_consp_without_lcons_or_monetary_id(self):

    lay = layers.Layers()  # Note that we do not pass any argument

    lay.cons = pd.DataFrame(index=['cons1', 'cons2', 'Total mass [kg]'],
                                columns=['subst1', 'subst2']).fillna(1.0)

    pdt.assert_frame_equal(lay.cons, lay.consp, check_like=True)

def read_tbl(tbl, dtype=None):

    if dtype is None:
        dtype = 'object'

    # Remove any decorative line
    tbl = re.sub('-[-+]+-\n', '', tbl)

    df =  pd.read_csv(StringIO(tbl), index_col=0, sep='|')

    # remove any leading or trailing spaces from index and columns labels
    df.index = df.index.str.strip()
    df.index.name.strip()
    df.columns = df.columns.str.strip()

    df = df.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    if dtype == 'float':  #todo: generalize, any dtype that starts with float really
        df = df.apply(pd.to_numeric)
    else:
        # Convert to right datatype (float, int, etc.)
        df = df.astype(dtype)

    ## If any remaining "object" datatype (i.e., string), also strip
    #if 'object' in set(df.dtypes.astype(str)):
    #    df = df.apply(lambda x: x.str.strip() if x.dtype == "object" else x)

    return df


def to_table_command(df, name=''):
    
    # Convert dataframe
    tbl = tabulate.tabulate(df, tablefmt='presto', headers=df.columns)

    
    # Extract type information
    dtypes = df.dtypes.astype('str')

    # Convert dtype data to string or dict 
    if len(set(dtypes)) == 1:
        dtypes = "'" + dtypes[0] + "'"
    else:
        dtypes = dtypes.to_dict()

    # put together command
    table_command ='{} = read_tbl(""" \n{}\n""", dtype={})'.format( name, tbl, dtypes)
    
    print(table_command)
    
    return table_command


if __name__ == '__main__':
    unittest.main()
